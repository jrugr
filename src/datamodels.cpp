////////////////////////////////////////
//  Copyright : GPL                   //
////////////////////////////////////////
#include "datamodels.h"


////////////////////////////////////////////////////////////////////////////////
QVariant SrcLayoutModel::headerData (int section, Qt::Orientation orientation, int role) const {
  if (role != Qt::DisplayRole) return QVariant();
  if (orientation == Qt::Horizontal) {
    switch (section) {
      case 1: return tr("Layout Name");
      case 2: return tr("Map");
    }
    return QString("");
  }
  return QVariant();
}


QVariant SrcLayoutModel::data (const QModelIndex &index, int role) const {
  QSvgRenderer flagSVG;
  QIcon icons;
  QHash<QString, QString> layouts;
  QString layout, PNG, SVG;
  int col, row;
  bool isPNG, isSVG;
  //
  if (!index.isValid()) return QVariant();
  col = index.column();
  row = index.row();
  layouts = mRules->layouts;
  layout = mLayoutKeys[row];
  PNG = mIconDir+"/"+fixLayoutName(layout)+".png";
  SVG = mIconDir+"/"+fixLayoutName(layout)+".svg";
  isPNG = QFile::exists(PNG);
  isSVG = QFile::exists(SVG);
  if (isSVG) {
    flagSVG.load(SVG);
    isSVG = flagSVG.isValid();
  }
  //
  if (isSVG) {
    QPixmap pix(48, 22);
    {
      QPainter painter;
      //
      painter.begin(&pix);
      flagSVG.render(&painter, QRectF(0, 0, 48, 22));
      painter.end();
    }
    icons = QIcon(pix);
  } else if (isPNG) {
    icons = QIcon(PNG);
  }
  //
  switch (role) {
    case Qt::TextAlignmentRole: return int(Qt::AlignLeft | Qt::AlignVCenter);
    case Qt::DecorationRole:
      if (col == LAYOUT_COLUMN_FLAG) return QIcon::fromTheme(layout, icons).pixmap(QSize(48, 32));
      break;
    case Qt::DisplayRole:
      switch (col) {
        case LAYOUT_COLUMN_NAME: return layouts.value(layout);
        case LAYOUT_COLUMN_MAP: return layout;
        default: break;
      }
      break;
    default: break;
  }
  return QVariant();
}


Qt::ItemFlags SrcLayoutModel::flags (const QModelIndex &index) const {
  Qt::ItemFlags defaultFlags = QAbstractTableModel::flags(index);
  return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | defaultFlags;
}


QMimeData *SrcLayoutModel::mimeData (const QModelIndexList &indexes) const {
  QMimeData *mimeData = QAbstractTableModel::mimeData(indexes);
  mimeData->setText(indexes.first().data().toString());
  return mimeData;
}


bool SrcLayoutModel::dropMimeData (const QMimeData *, Qt::DropAction action, int, int, const QModelIndex &) {
  if (action == Qt::IgnoreAction) return false;
  emit layoutRemoved();
  return true;
}


////////////////////////////////////////////////////////////////////////////////
QVariant DstLayoutModel::headerData (int section, Qt::Orientation orientation, int role) const {
  if (role != Qt::DisplayRole) return QVariant();
  if (orientation == Qt::Horizontal) {
    switch (section) {
      case 1: return tr("Layout Name");
      case 2: return tr("Map");
      case 3: return tr("Variant");
      case 4: return tr("Label");
    }
    return QString("");
  }
  return QVariant();
}


QVariant DstLayoutModel::data (const QModelIndex &index, int role) const {
  QHash<QString, QString> layouts;
  LayoutUnit lu;
  int col, row;
  //
  if (!index.isValid()) return QVariant();
  col = index.column();
  row = index.row();
  layouts = mRules->layouts;
  lu = mJrugr->layouts[row];
  switch (role) {
    case Qt::TextAlignmentRole: return int(Qt::AlignLeft | Qt::AlignVCenter);
    case Qt::DecorationRole:
      if (col == LAYOUT_COLUMN_FLAG) {
        return QIcon::fromTheme(lu.layout, QIcon(mIconDir+"/"+fixLayoutName(lu.layout)+".png")).pixmap(QSize(48, 32));
      }
      break;
    case Qt::DisplayRole:
      switch (col) {
        case LAYOUT_COLUMN_NAME: return layouts[lu.layout];
        case LAYOUT_COLUMN_MAP: return lu.layout;
        case LAYOUT_COLUMN_VARIANT: return lu.variant;
        case LAYOUT_COLUMN_DISPLAY_NAME: return lu.getDisplayName();
        default: break;
      }
      break;
    default: break;
  }
  return QVariant();
}


bool DstLayoutModel::dropMimeData (const QMimeData *, Qt::DropAction action, int, int, const QModelIndex &) {
  if (action == Qt::IgnoreAction) return false;
  emit layoutAdded();
  return true;
}


Qt::ItemFlags DstLayoutModel::flags (const QModelIndex &index) const {
  Qt::ItemFlags defaultFlags = QAbstractTableModel::flags(index);
  //
  if (mJrugr->layouts.size() >= 4) return Qt::ItemIsDragEnabled | defaultFlags;
  return Qt::ItemIsDropEnabled | Qt::ItemIsDragEnabled | defaultFlags;
}


////////////////////////////////////////////////////////////////////////////////
QVariant XkbOptionsModel::data (const QModelIndex &index, int role) const {
  int row, groupRow;
  QString xkbGroupNm;
  const XkbOptionGroup *xkbGroup;
  //
  if (!index.isValid()) return QVariant();
  row = index.row();
  switch (role) {
    case Qt::DisplayRole:
      if (!index.parent().isValid()) return mRules->optionGroups.values()[row].description;
      groupRow = index.parent().row();
      xkbGroupNm = mRules->optionGroups.keys()[groupRow];
      xkbGroup = &mRules->optionGroups[xkbGroupNm];
      return xkbGroup->options[row].description;
    case Qt::CheckStateRole:
      if (index.parent().isValid()) {
        groupRow = index.parent().row();
        xkbGroupNm = mRules->optionGroups.keys()[groupRow];
        xkbGroup = &mRules->optionGroups[xkbGroupNm];
        return mXkbConfig->options.indexOf(xkbGroup->options[row].name) == -1 ? Qt::Unchecked : Qt::Checked;
      }
      break;
    default: break;
  }
  return QVariant();
}


int XkbOptionsModel::rowCount (const QModelIndex &parent) const {
  if (!parent.isValid()) return mRules->optionGroups.count();
  if (!parent.parent().isValid()) return mRules->optionGroups.values()[parent.row()].options.count();
  return 0;
}


QModelIndex XkbOptionsModel::parent (const QModelIndex &index) const {
  if (!index.isValid()) return QModelIndex();
  if (index.internalId() < 100) return QModelIndex();
  return createIndex(((index.internalId()-index.row())/100)-1, index.column());
}


QModelIndex XkbOptionsModel::index (int row, int column, const QModelIndex &parent) const {
  if (!parent.isValid()) return createIndex(row, column);
  return createIndex(row, column, (100*(parent.row()+1))+row);
}


Qt::ItemFlags XkbOptionsModel::flags (const QModelIndex &index) const {
  if (!index.isValid()) return 0;
  if (!index.parent().isValid()) return Qt::ItemIsEnabled;
  return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
}


bool XkbOptionsModel::setData (const QModelIndex &index, const QVariant &value, int role) {
  QString xkbGroupNm;
  const XkbOptionGroup *xkbGroup;
  const XkbOption *option;
  int groupRow = index.parent().row();
  //
  if (groupRow < 0) return false;
  xkbGroupNm = mRules->optionGroups.keys()[groupRow];
  xkbGroup = &mRules->optionGroups[xkbGroupNm];
  option = &xkbGroup->options[index.row()];
  if (value.toInt() == Qt::Checked) {
    if (xkbGroup->exclusive) {
      // clear if exclusive (TODO: radiobutton)
      int idx = mXkbConfig->options.indexOf(QRegExp(xkbGroupNm+".*"));
      if (idx >= 0) {
        for (int f = 0; f < xkbGroup->options.count(); ++f) {
          if (xkbGroup->options[f].name == mXkbConfig->options[idx]) {
            setData(createIndex(f, index.column(), (quint32)index.internalId()-index.row()+f), Qt::Unchecked, role);
            break;
          }
        }
        //m_kxkbConfig->m_options.removeAt(idx);
        //idx = m_kxkbConfig->m_options.indexOf(QRegExp(xkbGroupNm+".*"));
      }
    }
    if (mXkbConfig->options.indexOf(option->name) < 0) mXkbConfig->options.append(option->name);
  } else {
    mXkbConfig->options.removeAll(option->name);
  }
  emit dataChanged(index, index);
  return true;
}


void XkbOptionsModel::gotoGroup (const QString &group, QTreeView *view) {
  int index = mRules->optionGroups.keys().indexOf(group);
  //
  if (index != -1) {
    QModelIndex modelIdx = createIndex(index, 0);
    view->setExpanded(modelIdx, true);
    view->scrollTo(modelIdx, QAbstractItemView::PositionAtTop);
    view->selectionModel()->setCurrentIndex(modelIdx, QItemSelectionModel::Current);
    view->setFocus(Qt::OtherFocusReason);
  } else {
    qDebug() << "XkbOptionsModel:can't scroll to group" << group;
  }
}
