////////////////////////////////////////
//  Copyright : GPL                   //
////////////////////////////////////////
#include "jrugr.h"


////////////////////////////////////////////////////////////////////////////////
static Atom
  atomActWin = None,
  atomClientList = None,
  atomCurDesk = None;


////////////////////////////////////////////////////////////////////////////////
static inline bool isMPlayer (const QString &name) {
  return (
    name.startsWith("mplayer", Qt::CaseInsensitive) ||
    name.startsWith("mplayer2", Qt::CaseInsensitive) ||
    name.startsWith("mpv", Qt::CaseInsensitive) ||
    name.endsWith(" - mpv", Qt::CaseInsensitive)
  );
}


static inline bool isMPlayerClass (const QString &name) {
  return (name == "xv" || name == "gl" || name == "vdpau");
}


////////////////////////////////////////////////////////////////////////////////
static bool makeFacehugger (Window w) {
  //WARNING! this WILL NOT work as expected on our own windows!
  return XSelectInput(QX11Info::display(), w, /*SubstructureNotifyMask |*/ StructureNotifyMask) == Success;
}


static void dumpWinInfo (const char *msg, Window w) {
  Window pw;
  //
  if (w == None) return;
  pw = X11Tools::windowTransientFor(w);
  qDebug() << msg << (int)(w) <<
    "class:" << X11Tools::windowClass(w) <<
    "name:" << X11Tools::windowName(w) <<
    "pid:" << X11Tools::windowPID(w) <<
    "transientfor:" << pw;
  //dumpWinInfo(" parent (tr)", pw);
}


////////////////////////////////////////////////////////////////////////////////
Jrugr::Jrugr (int &argc, char **argv) :
  QApplication(argc, argv),
  //
  mActiveGroup(0),
  mXkb(0),
  mTrayIcon(0),
  mTrayMenu(0),
  jCfg(0),
  mDeskJustChanged(false)
{
  if (atomActWin == None) atomActWin = XInternAtom(QX11Info::display(), "_NET_ACTIVE_WINDOW", True);
  if (atomClientList == None) atomClientList = XInternAtom(QX11Info::display(), "_NET_CLIENT_LIST", True);
  if (atomCurDesk == None) atomCurDesk = XInternAtom(QX11Info::display(), "_NET_CURRENT_DESKTOP", True);
  // mandatory to keep it running after closing about dialogs...
  setQuitOnLastWindowClosed(false);
  setWindowIcon(QIcon(":/about/jrugr.png"));
  //
  mCfgFileName = QDir::homePath()+"/.config/jrugr.cfg";
}


Jrugr::~Jrugr () {
  delete mXkb;
  delete mTrayMenu;
  delete jCfg;
  //! \warning the trayIcon *has* to be deleted here to prevent XFreeColormap() free corruption.
  delete mTrayIcon;
}


bool Jrugr::firstStart () {
  if (!QFile::exists(mCfgFileName)) {
    QString themePath;
    //
    qDebug() << " Jrugr:Config file not found in:" << mCfgFileName;
    QSettings jrugr(mCfgFileName, QSettings::IniFormat);
    QDir themeDir("/usr/share/jrugr/theme/default");
    if (themeDir.exists()) themePath = themeDir.path();
    else themePath = QCoreApplication::applicationDirPath()+"/theme/default";;
    jrugr.beginGroup("Style");
    mLangThemePath = themePath;
    jrugr.setValue("path", mLangThemePath);
    jrugr.endGroup(); //Style
    return true;
  }
  return false;
}


void Jrugr::startup () {
  if (firstStart()) configure();
  //
  {
    QSettings jrugr(mCfgFileName, QSettings::IniFormat, this);
    jrugr.beginGroup("Style");
    mLangThemePath = jrugr.value("path").toString()+"/language/";
    jrugr.endGroup(); //Style
  }
  jCfg = JrugrCfg::load(mCfgFileName);
  if (!jCfg) abort();
  if (jCfg->workMode == USE_XKB) configureXkb();
  mMplayerHack = jCfg->mplayerHack;
  //
  mXkb = new XKeyboard();
  mXkb->groupInfo(mGroupInfo);
  //
  connect(mXkb, SIGNAL(groupChanged(int)), this, SLOT(onGroupChanged(int)), Qt::QueuedConnection);
  connect(mXkb, SIGNAL(layoutChanged()), this, SLOT(onLayoutChanged()), Qt::QueuedConnection);
  connect(this, SIGNAL(changeLayout(int)), this, SLOT(onChangeLayout(int)), Qt::QueuedConnection);
  //
  connect(this, SIGNAL(activeWindowChanged()), this, SLOT(doActiveWindowChanged()), Qt::QueuedConnection);
  connect(this, SIGNAL(activeDesktopChanged()), this, SLOT(doActiveDesktopChanged()), Qt::QueuedConnection);
  connect(this, SIGNAL(clientListChanged()), this, SLOT(onClientListChanged()), Qt::QueuedConnection);
  //
#ifdef K8_MPLAYER_HACK
  mMPFix = new QTimer(this);
  mMPFix->setSingleShot(true);
  connect(mMPFix, SIGNAL(timeout()), this, SLOT(onMPFix()), Qt::QueuedConnection);
#endif
}


void Jrugr::initialize () {
  mXkb->groupInfo(mGroupInfo);
  //
  mMplayerHack = jCfg->mplayerHack;
  mActiveGroup = mXkb->activeGroup();
  mActiveWindow = X11Tools::activeWindow();
  mCurDesk = X11Tools::activeDesktop();
  setDeskActiveWindow(mCurDesk, X11Tools::activeWindow());
  //
  mTrayIcon = new QSystemTrayIcon(this);
  connect(mTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(trayClicked(QSystemTrayIcon::ActivationReason)));
  //
  createMenu();
  buildIcon();
  //
  installFacehuggers();
}


void Jrugr::reconfigure () {
  JrugrCfg *newConf = JrugrCfg::load(mCfgFileName);
  //
  if (newConf) {
    QXkbLayoutList nl;
    //
    mXkb->groupInfo(nl);
    bool diffGI = nl.size() != mGroupInfo.size();
    //
    if (!diffGI) {
      for (int f = nl.size()-1; f >= 0; --f) if (nl[f].name != mGroupInfo[f].name || nl[f].sym != mGroupInfo[f].sym) { diffGI = true; break; }
    }
    //
    if (diffGI || newConf->layouts != jCfg->layouts || newConf->showFlag != jCfg->showFlag ||
        newConf->workMode != jCfg->workMode || newConf->switching != jCfg->switching ||
        mMplayerHack != newConf->mplayerHack) {
      delete jCfg;
      jCfg = newConf;
      mMplayerHack = jCfg->mplayerHack;
      mGroupInfo = nl;
      createMenu();
      buildIcon();
    } else {
      delete newConf;
    }
  }
}


int Jrugr::setKeyLayout (QString keyConf) {
  qDebug() << "Jrugr::setKeyLayout:" << keyConf;
  QStringList argument = keyConf.split(" ");
  qDebug() << " Jrugr:setxkbmap argumetns:" << argument;
  int result = QProcess::execute("setxkbmap", argument);
  qDebug() << " Jrugr:setxkbmap result:" << result;
  return result;
}


void Jrugr::configureXkb () {
  QString layout, variant, option;
  QString model = jCfg->model;
  //
  for (int f = 0; f < jCfg->layouts.count(); ++f) {
    layout += jCfg->layouts[f].layout;
    variant += jCfg->layouts[f].variant;
    if (f < jCfg->layouts.count()-1) { layout += ','; variant += ','; }
  }
  option = jCfg->options.join(",");
  qDebug() << "Set layout:" << layout;
  if (!(model.isEmpty() || model.isNull()) && !(layout.isEmpty() || layout.isNull())) {
    QString args = "-model "+model+" -layout "+layout;
    QString tmp = variant;
    if (!variant.isNull() && !variant.isEmpty() && !tmp.remove(",").isEmpty()) args +=" -variant "+variant;
    if (!option.isNull() && !option.isEmpty()) args +=" -option "+option;
    qDebug() << " Jrugr:XKB args " << args;
    if (setKeyLayout(args) == QProcess::CrashExit) {
      qDebug() << " Jrugr:XKB isn`t set";
      qDebug() << " Jrugr:------------------------------";
      return;
    }
    qDebug()  << " Jrugr:XKB  set";
  }
}


void Jrugr::configure () {
  JrugrConfigForm *mCfgForm = new JrugrConfigForm(mCfgFileName);
  connect(mCfgForm, SIGNAL(saveConfig()), SLOT(reconfigure()));
  mCfgForm->exec(); // the form will die on closing
}


int Jrugr::layIndex (Window w, const QString &propname, bool unknownIsZero) const {
  QString alay(X11Tools::getjprop(w, propname.toUtf8().constData()));
  //
  if (alay.size() > 0) {
    int idx = XKeyboard::findBySym(mGroupInfo, alay);
    //
    qDebug() << "Win:" << w << "alay:" << alay << "idx:" << idx;
    return idx;
  }
  return unknownIsZero ? 0 : -1;
}


bool Jrugr::isKnownWindow (Window w) const {
  return mKnownWindows.contains(w);
}


bool Jrugr::isKnownApp (Window w) const {
  return mKnownApps.contains(X11Tools::appWindow(w));
}


WinInfo *Jrugr::findWindowInfo (Window w) {
  return mKnownWindows.value(w, 0);
}


// find application info for the given window
WinInfo *Jrugr::findAppInfo (Window w) {
  return mKnownApps.value(X11Tools::appWindow(w), 0);
}


int Jrugr::desktopLayout (int desk) const {
  if (desk < 0 || desk >= mDeskLangs.size()) return 0;
  return mDeskLangs.at(desk);
}


void Jrugr::setDesktopLayout (int desk, int group) {
  if (desk >= 0 && group < mGroupInfo.size()) {
    X11Tools::setjprop(None, QString("DESK%1_LAYOUT").arg(desk), mGroupInfo[group].sym);
  }
  if (desk < 0 || desk > 42) return; // invalid desktop number
  while (mDeskLangs.size() <= desk) {
    QString ln(QString("DESK%1_LAYOUT").arg(mDeskLangs.size()));
    int idx = layIndex(None, ln.toLatin1().constData(), true);
    //
    //qDebug() << "desk:" << mDeskLangs.size() << "layidx:" << idx;
    if (group < mGroupInfo.size()) X11Tools::setjprop(None, ln, mGroupInfo[group].sym);
    mDeskLangs << idx;
  }
  mDeskLangs[desk] = group;
}


Window Jrugr::deskActiveWindow (int desk) {
  if (desk < 0 || desk >= mDeskActiveWindows.size()) return None;
  return mDeskActiveWindows.at(desk);
}


void Jrugr::setDeskActiveWindow (int desk, Window w) {
  if (desk < 0 || desk > 42) return; // invalid desktop number
  while (mDeskActiveWindows.size() <= desk) mDeskActiveWindows << None;
  mDeskActiveWindows[desk] = w;
  if (w != None) {
    qDebug() << "DESK:" << desk <<
        "class:" << X11Tools::windowClass(w) <<
        "name:" << X11Tools::windowName(w);
  } else {
    qDebug() << "DESK:" << desk << "None";
  }
}


void Jrugr::installFacehuggers () {
  QX11WindowList wl = X11Tools::topLevelWindows();
  QSet<WId> qtwl;
  //
  {
    //QWidgetList twl = QApplication::allWidgets();
    QWidgetList twl = QApplication::topLevelWidgets();
    foreach (QWidget *wd, twl) {
      WId w = wd->effectiveWinId();
      if (w != None) qtwl << w;
    }
  }
  //
  for (int f = 0; f < wl.size(); ++f) {
    if (wl[f] != None && !qtwl.contains(wl[f]) && !isKnownWindow(wl[f])) {
      WinInfo *wi = new WinInfo;
      //
      wi->w = wl[f];
      wi->appWindow = X11Tools::appWindow(wl[f]);
      wi->layout = layIndex(wi->w, "WIN_LAYOUT", true);
      if (wi->layout < mGroupInfo.size()) X11Tools::setjprop(wi->w, "WIN_LAYOUT", mGroupInfo[wi->layout].sym);
      mKnownWindows[wl[f]] = wi;
      makeFacehugger(wi->w);
      //
      if (wi->appWindow == wi->w && !isKnownApp(wi->w)) {
        WinInfo *w2 = new WinInfo;
        //
        w2->appWindow = w2->w = wi->w;
        w2->layout = layIndex(w2->w, "APP_LAYOUT", false);
        if (w2->layout < 0) w2->layout = wi->layout;
        if (w2->layout < mGroupInfo.size()) X11Tools::setjprop(w2->w, "APP_LAYOUT", mGroupInfo[w2->layout].sym);
        mKnownApps[w2->w] = w2;
      }
    }
  }
}


void Jrugr::doActiveWindowChanged () {
  //qDebug() << "doActiveWindowChanged";
  onActiveWindowChanged(X11Tools::activeWindow());
}


void Jrugr::doActiveDesktopChanged () {
  //qDebug() << "doActiveDesktopChanged";
  onActiveDesktopChanged(X11Tools::activeDesktop());
}


void Jrugr::onLayoutChanged () {
  qDebug()<<"Jrugr::onLayoutChanged : reconfig";
  reconfigure();
  qDebug()<<"Jrugr::onLayoutChanged : done";
}


QIcon Jrugr::langIcon (const QString &lsym, int wdt, int hgt, bool noFlag) {
  if (!noFlag) {
    QString path = mLangThemePath+fixLayoutName(lsym, jCfg->useSU);
    QString SVGfile = path+".svg";
    //
    if (QFile::exists(SVGfile)) {
      QSvgRenderer flagSVG(SVGfile);
      if (flagSVG.isValid()) {
        QPixmap pix(wdt, hgt);
        QPainter painter;
        painter.begin(&pix);
        flagSVG.render(&painter, QRectF(0, 0, wdt, hgt));
        painter.end();
        return QIcon(pix);
      }
    }
    //
    QString PNGfile = path+".png";
    if (QFile::exists(PNGfile)) {
      return QIcon(PNGfile);
    }
  }
  //
  {
    qDebug() << ":::" << lsym;
    QFont font("Helvetica [Cronyx]", 15); //FIXME: should be configurable
    //QFont font("Tahoma", 15);
    font.setBold(true);
    font.setLetterSpacing(QFont::PercentageSpacing, 120);
    QPixmap pix(lsym.length()*20, 26);
    QPainter painter;
    painter.begin(&pix);
    painter.setFont(font);
    painter.fillRect(pix.rect(), Qt::darkGray);
    painter.setPen(Qt::white);
    painter.drawText(pix.rect(), Qt::AlignVCenter|Qt::AlignCenter, lsym);
    painter.end();
    return QIcon(pix);
  }
}


void Jrugr::createMenu () {
  if (!mTrayMenu) {
    mTrayMenu = new QMenu();
    connect(mTrayMenu, SIGNAL(triggered(QAction *)), SLOT(actionsActivate(QAction *)));
  } else {
    mTrayMenu->clear();
  }
  //
  for (int index = 0; index < mGroupInfo.size(); ++index) {
    QString lname = mGroupInfo[index].name, lsym = mGroupInfo[index].sym;
    //
    QAction *act = new QAction(lname, this);
    act->setIcon(langIcon(lsym));
    act->setData(lname);
    mTrayMenu->addAction(act);
  }
  //
  mTrayMenu->addSeparator();
  QAction *config = new QAction(tr("Configure"), this);
  config->setData("configure");
  mTrayMenu->addAction(config);
  //
  mTrayMenu->addSeparator();
  QAction *about_jrugr = new QAction(tr("About Jrugr"), this);
  about_jrugr->setData("about_jrugr");
  mTrayMenu->addAction(about_jrugr);
  //
  QAction *about_qt = new QAction(tr("About Qt"), this);
  about_qt->setData("about_qt");
  mTrayMenu->addAction(about_qt);
  //
  mTrayMenu->addSeparator();
  QAction *jrugrExit = new QAction(tr("Exit"), this);
  jrugrExit->setData("exit");
  mTrayMenu->addAction(jrugrExit);
}


void Jrugr::buildIcon () {
  if (!mTrayIcon) return;
  mTrayIcon->setIcon(langIcon(mGroupInfo[mActiveGroup].sym, 32, 22, !jCfg->showFlag));
  mTrayIcon->setToolTip(mGroupInfo[mActiveGroup].name);
  if (mTrayMenu) mTrayIcon->setContextMenu(mTrayMenu);
  mTrayIcon->show();
}


void Jrugr::trayClicked (QSystemTrayIcon::ActivationReason reason) {
  switch (reason) {
    case QSystemTrayIcon::Trigger: setNextGroup(); break;
    case QSystemTrayIcon::DoubleClick: setNextGroup(); break;
    case QSystemTrayIcon::MiddleClick: setPrevGroup(); break;
    default: ;
  }
}


// new layout activated; fix all necessary shit
void Jrugr::onGroupChanged (int index) {
  qDebug() << "Jrugr::onGroupChanged: index:" << index;
  mActiveGroup = index;
  buildIcon();
  //
  if (mActiveWindow != None) {
    WinInfo *wi = findWindowInfo(mActiveWindow);
    //
    if (!wi) {
      qDebug() << "new window";
      installFacehuggers();
      wi = findWindowInfo(mActiveWindow);
    }
    //
    if (wi) {
      qDebug() << "fixing window layout";
      wi->layout = index;
      if (index < mGroupInfo.size()) X11Tools::setjprop(wi->w, "WIN_LAYOUT", mGroupInfo[index].sym);
      if (jCfg->switching == APP_LAYOUT) {
        wi = findAppInfo(mActiveWindow);
        if (wi) {
          qDebug() << "fixing app layout";
          wi->layout = index;
          if (index < mGroupInfo.size()) X11Tools::setjprop(wi->w, "APP_LAYOUT", mGroupInfo[index].sym);
        } else {
          qWarning() << "***APP SHIT!***";
        }
      }
    } else {
      qWarning() << "***WINDOW SHIT!***";
    }
  }
  //
  setDesktopLayout(X11Tools::activeDesktop(), mActiveGroup);
}


void Jrugr::setNextGroup () {
  mXkb->setActiveGroup(mActiveGroup+1>=mGroupInfo.size()?0:mActiveGroup+1);
}


void Jrugr::setPrevGroup () {
  mXkb->setActiveGroup(mActiveGroup-1<0?mGroupInfo.size()-1:mActiveGroup-1);
}


void Jrugr::windowDies (Window w) {
  if (w != None) {
    qDebug("**************************** FACEHUGGER PARENT DIES ****************************");
    Window appW = None;
    // remove window
    {
      QWIHashMutableIterator i(mKnownWindows);
      while (i.hasNext()) {
        WinInfo *wi = i.next().value();
        //
        if (wi->w == w) {
          i.remove();
          if (wi->w == wi->appWindow) {
            if (appW == None) appW = wi->w;
            else if (appW != wi->w) qWarning("*** WTF?! ***");
          }
          delete wi;
        }
      }
    }
    if (appW != None) {
      // remove application
      QWIHashMutableIterator i(mKnownApps);
      while (i.hasNext()) {
        WinInfo *wi = i.next().value();
        //
        if (wi->w == appW) {
          i.remove();
          delete wi;
        }
      }
    }
  }
}


void Jrugr::onChangeLayout (int index) {
  qDebug() << "Jrugr::changeLayout:" << index;
  mXkb->setActiveGroup(index);
}


void Jrugr::actionsActivate (QAction *action) {
  QString cmd = action->data().toString();
  //
  qDebug() << "Jrugr::actionsActivate() command" << cmd;
  if (cmd == "configure") {
    configure();
  } else if (cmd == "about_qt") {
    aboutQt();
  } else if (cmd == "about_jrugr") {
    QMessageBox::about(0,
      tr("About Jrugr"),
      tr("<h2>Jrugr, the keyboard layout switcher</h2>"
         "<b>Version</b>: %1<p>"
         "Gui tool to configure XKB extentions of X server.<p>"
         "(c) 2009-2011 Fedor Chelbarakh, Petr Vanek, Ketmar").arg(JRUGR_VERSION));
  } else if (cmd == "exit") {
    qDebug() << "Jrugr::actionsActivate() exit";
    quit();
  } else {
    for (int f = mGroupInfo.size()-1; f >= 0; --f) {
      if (cmd == mGroupInfo[f].name) {
        emit changeLayout(f);
        break;
      }
    }
  }
}


#ifdef K8_MPLAYER_HACK
static bool isMPlayer (Window w) {
  if (w != None) {
    if (X11Tools::windowDesktop(w) == -1 && isMPlayerClass(X11Tools::windowClass(w))) {
      QString name = X11Tools::windowName(w).toLower();
      if (isMPlayer(name)) return true;
    }
  }
  return false;
}


static QX11WindowList getPossibleActives (Window mpw) {
  QX11WindowList res, wl = X11Tools::topLevelWindowStack();
  int desk = X11Tools::activeDesktop();
  Window wine = None;
  //
  qDebug() << "looking for window to refocus...";
  for (int f = 0; f < wl.size(); ++f) {
    Window w = wl.at(f);
    int wd = X11Tools::windowDesktop(w);
    //
    if (wd == desk) {
      // skip 'special' windows
      QStringList sl = X11Tools::windowStateNames(w);
      if (sl.indexOf("_NET_WM_STATE_STICKY") < 0 &&
          sl.indexOf("_NET_WM_STATE_HIDDEN") < 0 &&
          sl.indexOf("_NET_WM_STATE_SHADED") < 0 &&
          sl.indexOf("_NET_WM_STATE_SKIP_TASKBAR") < 0 &&
          sl.indexOf("_NET_WM_STATE_ABOVE") < 0 &&
          sl.indexOf("_NET_WM_STATE_BELOW") < 0) {
        QString cls(X11Tools::windowClass(w));
        if (isMPlayerClass(cls)) {
          QString name(X11Tools::windowName(w).toLower());
          if (isMPlayer(name)) continue;
        }
        if (wine == None && cls == "explorer.exe") {
          wine = w;
          qDebug() << " WINE:" << cls;
        } else {
          //FIXME: this will not work as expected!
          if (f > 0 && wl.at(f-1) == mpw) {
            res.prepend(w);
          } else {
            res << w;
          }
          qDebug() << " NORM:" << cls;
        }
      }
    }
  }
  //
  if (wine != None) res << wine;
  //
  return res;
}


bool Jrugr::deactivateMPlayer () {
  qDebug() << "onActiveWindowChanged";
  Window aw = X11Tools::activeWindow();
  bool changeFocus = false;
  //
  if (aw != None) {
    if (X11Tools::windowDesktop(aw) == -1 && isMPlayerClass(X11Tools::windowClass(aw))) {
      QString name = X11Tools::windowName(aw).toLower();
      if (isMPlayer(name)) {
        qDebug() << "MPlayer hack!";
        changeFocus = true;
      }
    }
  } else {
    qDebug() << "Empty window hack!";
    changeFocus = true;
  }
  //
  if (changeFocus) {
    // find previous window
    Window pw = deskActiveWindow(mCurDesk);
    int dsk = X11Tools::windowDesktop(pw);
    if (pw == None || isMPlayer(pw) || (dsk != -1 && dsk != mCurDesk)) {
      QX11WindowList wl = getPossibleActives(aw);
      //
      if (wl.size() > 0) pw = wl.at(wl.size()-1); else pw = None;
    }
    if (pw != None) {
      qDebug() << "CHANGING FOCUS!" <<
        "class:" << X11Tools::windowClass(pw) <<
        "name:" << X11Tools::windowName(pw) <<
        "pid:" << X11Tools::windowPID(pw);
      //
      X11Tools::setActiveWindow(pw);
      return true;
    }
  }
  return false;
}


void Jrugr::onMPFix () {
  if (!deactivateMPlayer()) setDeskActiveWindow(mCurDesk, mActiveWindow);
}
#endif


void Jrugr::onActiveWindowChanged (Window w) {
  qDebug() << "onActiveWindowChanged";
  if (w == None) {
#ifdef K8_MPLAYER_HACK
    if (mDeskJustChanged) {
      //deactivateMPlayer();
      if (mMplayerHack) {
        mMPFix->start(MPLAYER_HACK_TIMEOUT);
      }
      mDeskJustChanged = false;
    }
#else
    setDeskActiveWindow(mCurDesk, w);
#endif
    mActiveWindow = None;
    qDebug() << "new active window: NONE";
    return;
  }
  qDebug() << "new active window:" << (int)w << "desktop:" << X11Tools::windowDesktop(w);
  dumpWinInfo("new active window:", w);
  //
  if (mDeskJustChanged) {
    qDebug() << "*** mDeskJustChanged ***";
    mDeskJustChanged = false;
#ifdef K8_MPLAYER_HACK
    if (mMplayerHack && isMPlayer(w)) {
      //mMPFix->start(MPLAYER_HACK_TIMEOUT);
      if (deactivateMPlayer()) return;
    }
#endif
  }
  //
#ifdef K8_MPLAYER_HACK
  if (!mMplayerHack || !isMPlayer(w))
#endif
  setDeskActiveWindow(mCurDesk, w);
  {
    WinInfo *wi = findWindowInfo(w), *aw;
    if (!wi) {
      qDebug() << "new window";
      installFacehuggers();
      wi = findWindowInfo(w);
    }
    if (wi) {
      switch (jCfg->switching) {
        case APP_LAYOUT:
          if ((aw = findAppInfo(w)) != findAppInfo(mActiveWindow)) {
            qDebug("application changed!");
            if (aw) {
              if (aw->layout != mActiveGroup) {
                qDebug("enforcing layout %d", aw->layout);
                emit changeLayout(aw->layout);
              }
            } else {
              qWarning("*** APP SHIT! ***");
            }
          }
          break;
        case WIN_LAYOUT:
          if (wi->layout != mActiveGroup) {
            qDebug("enforcing layout %d", wi->layout);
            emit changeLayout(wi->layout);
          }
          break;
        default: break;
      }
    }
  }
  mActiveWindow = w;
}


void Jrugr::onClientListChanged () {
  qDebug() << "onClientListChanged";
  installFacehuggers();
}


void Jrugr::onActiveDesktopChanged (int desk) {
  qDebug() << "onActiveDesktopChanged:" << desk;
  mDeskJustChanged = true;
#ifdef K8_MPLAYER_HACK
  //if (deactivateMPlayer()) mDeskJustChanged = false;
  if (mMplayerHack) {
    mMPFix->start(MPLAYER_HACK_TIMEOUT);
  }
#endif
  if (jCfg->switching == DESK_LAYOUT && mCurDesk != desk) {
    int ll = desktopLayout(desk);
    setDesktopLayout(desk, ll); // expand desktop list
    if (ll != mActiveGroup) {
      qDebug("enforce layout %d for desktop %d", ll, desk);
      emit changeLayout(mDeskLangs.at(desk));
    }
  }
  mCurDesk = desk;
}


bool Jrugr::x11EventFilter (XEvent *event) {
  //
  //qDebug() << "Jrugr::x11EventFilter:" << x11EventName(event);
  switch (event->type) {
    case DestroyNotify:
      windowDies(event->xdestroywindow.window);
      break;
    case PropertyNotify:
      if (event->xproperty.state == PropertyNewValue) {
        if (atomActWin != None && event->xproperty.atom == atomActWin) {
          //qDebug() << "emit: activeWindowChanged";
          emit activeWindowChanged();
        }
        if (atomCurDesk != None && event->xproperty.atom == atomCurDesk) {
          //qDebug() << "emit: activeDesktopChanged";
          emit activeDesktopChanged();
        }
        if (atomClientList != None && event->xproperty.atom == atomClientList) {
          qDebug() << "emit: clientListChanged";
          emit clientListChanged();
        }
      }
      //qDebug() << "PropertyNotify complete";
      break;
    default:
      if (mXkb) mXkb->processEvent(event);
      //return false;
      break;
  }
  return false; // normal dispatching
}
