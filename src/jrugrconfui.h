////////////////////////////////////////
//  Copyright : GPL                   //
////////////////////////////////////////
#ifndef JRUGRCONFIG_H
#define JRUGRCONFIG_H

#include "jrugrdefs.h"
#include "x11tools.h"
#include "datamodels.h"
#include "ui_configwin.h"


class JrugrConfigForm : public QDialog {
  Q_OBJECT

public:
  JrugrConfigForm (const QString &optFileName, QWidget *parent=0);
  ~JrugrConfigForm ();

  void getHotKeys (XEvent *event);
  void clearHotKeys ();

protected:
  void closeEvent (QCloseEvent *event);

signals:
  void saveConfig ();

public slots:
  void addLayout ();
  void delLayout ();
  void srcClick (QModelIndex index);
  void dstClick ();
  void comboModelCh (int index);
  void comboVariantCh (int index);
  void statSelect (bool check);
  void apply ();
  void setFlagUse ();
  void setMPHack ();
  void layoutUp ();
  void layoutDown ();
  void updateOptionsCommand ();
  void xkbOptionsChanged (const QModelIndex &topLeft, const QModelIndex &bottomRight);
  void statSwitching (bool checked);

private:
  bool loadRules ();
  void setCmdLine ();
  bool setStat ();
  void initXKBTab ();
  int getSelectedDstLayout ();

private:
  Ui_ConfigForm cfgUI;

private:
  QString mTheme;
  QStringList mModelList;
  RulesInfo *mCurRule;
  SrcLayoutModel *mSrcLayoutModel;
  DstLayoutModel *mDstLayoutModel;
  XkbOptionsModel *mXkbOptionsModel;
  JrugrCfg *mJCfg;
  QList<XkbVariant> mVariants;
  QString mIcoPath;
  QString mKey;
  QString mMods;
  QString mHotKeys;
  QString mCfgFileName;
};


#endif
