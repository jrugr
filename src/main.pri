QT += gui

DEPENDPATH += $$PWD
INCLUDEPATH += $$PWD


HEADERS += \
  $$PWD/jrugr.h \
  $$PWD/xkeyboard.h \
  $$PWD/jcfg.h \
  $$PWD/jrugrconfui.h \
  $$PWD/x11tools.h \
  $$PWD/datamodels.h \
  $$PWD/jrugrdefs.h \


SOURCES += \
  $$PWD/jrugr.cpp \
  $$PWD/xkeyboard.cpp \
  $$PWD/jcfg.cpp \
  $$PWD/jrugrconfui.cpp \
  $$PWD/x11tools.cpp \
  $$PWD/main.cpp \
  $$PWD/datamodels.cpp \


FORMS += \
  $$PWD/ui/configwin.ui \
