////////////////////////////////////////
//  Copyright : GPL                   //
////////////////////////////////////////
#include "jcfg.h"


////////////////////////////////////////////////////////////////////////////////
JrugrCfg *JrugrCfg::load (const QString &fname) {
  JrugrCfg *res = new JrugrCfg();
  LayoutUnit lu;
  QSettings qcfg(fname, QSettings::IniFormat);
  //
  qcfg.beginGroup("KeyLayout");
  res->model = qcfg.value("model").toString();
  if (res->model.isEmpty()) res->model = "pc104";
  if (!qcfg.value("layout").toString().isEmpty()) {
    QStringList
      l = qcfg.value("layout").toString().split(","),
      v = qcfg.value("variant").toString().split(",");
    //
    for (int f = 0; f < l.size(); ++f) {
      lu.layout = l[f];
      if (v.size() < f) lu.variant = ""; else lu.variant = v[f];
      res->layouts.append(lu);
    }
  } else {
    lu.layout = "us";
    lu.variant = "";
    res->layouts.append(lu);
  }
  if (!qcfg.value("option").toString().isEmpty()) res->options = qcfg.value("option").toString().split(",");
  //
  res->workMode = (JWorkMode)qcfg.value("workMode", ONLY_INDICATION).toInt();
  if (res->workMode < ONLY_INDICATION || res->workMode > USE_XKB) res->workMode = ONLY_INDICATION;
  //
  res->switching = (JSwitchMode)qcfg.value("switching", GLOBAL_LAYOUT).toInt();
  if (res->switching < GLOBAL_LAYOUT || res->switching > DESK_LAYOUT) res->switching = GLOBAL_LAYOUT;
  //
  res->showFlag = qcfg.value("showflag", true).toBool();
  res->useSU = qcfg.value("useSU", true).toBool();
  res->mplayerHack = qcfg.value("mplayerHack", false).toBool();
  qcfg.endGroup();
  return res;
}


bool JrugrCfg::save (const QString &fname) const {
  LayoutUnit lu;
  QSettings qcfg(fname, QSettings::IniFormat);
  QString layout, variant;
  //
  qcfg.beginGroup("KeyLayout");
  qcfg.setValue("model", model);
  //
  for (int f = 0; f < layouts.size(); ++f) {
    layout += layouts[f].layout;
    if (f < layouts.size()-1) layout += ",";
    variant += layouts[f].variant;
    if (f < layouts.size()-1) variant += ",";
  }
  qcfg.setValue("layout", layout);
  qcfg.setValue("variant", variant);
  qcfg.setValue("workMode", workMode);
  qcfg.setValue("showflag", showFlag);
  qcfg.setValue("switching", switching);
  qcfg.setValue("useSU", useSU);
  qcfg.setValue("mplayerHack", mplayerHack);
  qcfg.setValue("option", options.join(","));
  qcfg.endGroup();
  return true;
}
