////////////////////////////////////////
//  Copyright : GPL                   //
////////////////////////////////////////
#include "x11tools.h"


//static const char *OPTIONS_SEPARATOR = ",";


// compiler will size array automatically
static const char *X11DirList[] = {
  "/etc/X11/",
  "/usr/share/X11/",
  "/usr/local/share/X11/",
  "/usr/X11R6/lib/X11/",
  "/usr/X11R6/lib64/X11/",
  "/usr/local/X11R6/lib/X11/",
  "/usr/local/X11R6/lib64/X11/",
  "/usr/lib/X11/",
  "/usr/lib64/X11/",
  "/usr/local/lib/X11/",
  "/usr/local/lib64/X11/",
  "/usr/pkg/share/X11/",
  "/usr/pkg/xorg/lib/X11/",
};


// compiler will size array automatically.
static const char *rulesFileList[] = {
  "xkb/rules/base",
  "xkb/rules/xorg",
  "xkb/rules/xfree86"
};


// Macro will return number of elements in any static array as long as the
// array has at least one element.
#define ARRAY_SIZE(array) (sizeof(array)/sizeof(array[0]))


static const int X11_DIR_COUNT = ARRAY_SIZE(X11DirList);
static const int X11_RULES_COUNT = ARRAY_SIZE(rulesFileList);


////////////////////////////////////////////////////////////////////////////////
static char *getStringProperty (Window w, Atom qAtom, int *inutf8) {
  Atom aType;
  int format;
  unsigned long itemCount;
  unsigned long bytesAfter;
  unsigned char *data = NULL;
  static Atom utf8A = None, utf8SA = None, strA = None;
  int status;
  Display *dpy = QX11Info::display();
  //
  if (inutf8) *inutf8 = 0;
  if (qAtom == None || w == None) return NULL;
  if (utf8A == None) utf8A = XInternAtom(dpy, "UTF-8", True); // some dumb WMs using this type
  if (utf8SA == None) utf8SA = XInternAtom(dpy, "UTF8_STRING", True);
  if (strA == None) strA = XInternAtom(dpy, "STRING", True);
  //
  if (utf8SA != None) {
    status = XGetWindowProperty(dpy, w, qAtom, 0, 0x77777777, False, utf8SA, &aType, &format, &itemCount, &bytesAfter, (unsigned char **)&data);
    if (status >= Success && itemCount > 0 && data != NULL) {
      if (inutf8) *inutf8 = 1;
      return (char *)data;
    }
    if (data != NULL) XFree(data);
  }
  //
  if (utf8A != None) {
    status = XGetWindowProperty(dpy, w, qAtom, 0, 0x77777777, False, utf8A, &aType, &format, &itemCount, &bytesAfter, (unsigned char **)&data);
    if (status >= Success && itemCount > 0 && data != NULL) {
      if (inutf8) *inutf8 = 1;
      return (char *)data;
    }
    if (data != NULL) XFree(data);
  }
  //
  if (strA != None) {
    status = XGetWindowProperty(dpy, w, qAtom, 0, 0x77777777, False, strA, &aType, &format, &itemCount, &bytesAfter, (unsigned char **)&data);
    if (status >= Success && itemCount > 0 && data != NULL) return (char *)data;
    if (data != NULL) XFree(data);
  }
  //
  status = XGetWindowProperty(dpy, w, qAtom, 0, 0x77777777, False, AnyPropertyType, &aType, &format, &itemCount, &bytesAfter, (unsigned char **)&data);
  if (status >= Success && itemCount > 0 && data != NULL) return (char *)data;
  if (data != NULL) XFree(data);
  return NULL;
}


static QString getStrProp (Window w, Atom qAtom) {
  char *s;
  int inUtf = 0;
  //
  s = getStringProperty(w, qAtom, &inUtf);
  if (s == NULL) return QString();
  if (inUtf) {
    QString res(QString::fromUtf8(s));
    XFree(s);
    return res;
  } else {
    QString res(QString::fromLatin1(s));
    XFree(s);
    return res;
  }
}


////////////////////////////////////////////////////////////////////////////////
const QString X11Tools::findX11Dir () {
  /* find directory whith xkb config files */
  for (int f = 0; f < X11_DIR_COUNT; ++f) {
    const char *xDir = X11DirList[f];
    if (xDir != NULL && QDir(QString(xDir)+"xkb/rules").exists()) return QString(xDir);
  }
  return QString();
}


const QString X11Tools::findXkbRulesFile (const QString &x11Dir, Display *dpy) {
  QString rulesFile;
  XkbRF_VarDefsRec vd;
  char *tmp = NULL;
  //
  if (XkbRF_GetNamesProp(dpy, &tmp, &vd) && tmp != NULL) {
    //qDebug() << "namesprop " << tmp;
    rulesFile = x11Dir+QString("xkb/rules/%1").arg(tmp);
    //qDebug() << "rulesF " << rulesFile;
  } else {
    // old way
    for (int f = 0; f < X11_RULES_COUNT; ++f) {
      const char *ruleFile = rulesFileList[f];
      QString xruleFilePath = x11Dir+ruleFile;
      qDebug() << "trying xrules path " << xruleFilePath;
      if (QFile(xruleFilePath).exists()) { rulesFile = xruleFilePath; break; }
    }
  }
  delete tmp;
  return rulesFile;
}


/* load the list of languages, keybord layouts and option supported by xkb extension X Server and save into hash table */
RulesInfo *X11Tools::loadRules (const QString &file, bool layoutsOnly) {
  XkbRF_RulesPtr xkbRules = XkbRF_Load(QFile::encodeName(file).data(), (char *)"", true, true);
  //
  qDebug() << " X11Tools::loadRule ";
  if (xkbRules == NULL) return 0; // throw Exception
  // try to translate layout names by countries in desktop_kdebase
  // this is poor man's translation as it's good only for layout names and only those which match country names
  RulesInfo *rulesInfo = new RulesInfo();
  //
  for (int f = 0; f < xkbRules->layouts.num_desc; ++f) {
    QString layoutName(xkbRules->layouts.desc[f].name);
    rulesInfo->layouts.insert(layoutName, xkbRules->layouts.desc[f].desc);
  }
  //
  if (layoutsOnly == true) { XkbRF_Free(xkbRules, true); return rulesInfo; }
  //
  for (int f = 0; f < xkbRules->models.num_desc; ++f) {
    rulesInfo->models.insert(xkbRules->models.desc[f].name, QString(xkbRules->models.desc[f].desc));
  }
  //
  for (int f = 0; f < xkbRules->options.num_desc; ++f) {
    QString optionName = xkbRules->options.desc[f].name;
    //qDebug() << " option: " << optionName;
    int colonPos = optionName.indexOf(':');
    QString groupName = optionName.mid(0, colonPos);
    //
    if (colonPos != -1) {
      XkbOption option;
      //
      //qDebug() << " option: " << optionName;
      if (!rulesInfo->optionGroups.contains(groupName)) {
        rulesInfo->optionGroups.insert(groupName, createMissingGroup(groupName));
        //qDebug() << " added missing option group: " << groupName;
      }
      option.name = optionName;
      option.description = xkbRules->options.desc[f].desc;
      //option.group = &rulesInfo->optionGroups[groupName];
      rulesInfo->optionGroups[groupName].options <<option;
      rulesInfo->options.insert(optionName, option);
    } else {
      XkbOptionGroup optionGroup;
      //
      if (groupName == "Compose") groupName = "compose";
      if (groupName == "compat") groupName = "numpad";
      optionGroup.name = groupName;
      optionGroup.description = xkbRules->options.desc[f].desc;
      optionGroup.exclusive = isGroupExclusive(groupName);
      //qDebug() << " option group: " << groupName;
      rulesInfo->optionGroups.insert(groupName, optionGroup);
    }
  }
  //
  XkbRF_Free(xkbRules, true);
  return rulesInfo;
}


XkbOptionGroup X11Tools::createMissingGroup (const QString &groupName) {
  // workaround for empty 'compose' options group description
  XkbOptionGroup optionGroup;
  optionGroup.name = groupName;
  //optionGroup.description = "";
  optionGroup.exclusive = isGroupExclusive(groupName);
  return optionGroup;
}


bool X11Tools::isGroupExclusive (const QString &groupName) {
  return
    groupName == "ctrl" || groupName == "keypad" || groupName == "nbsp" ||
    groupName == "kpdl" || groupName == "caps" || groupName == "altwin";
}


/* pretty simple algorithm - reads the layout file and
 *   tries to find "xkb_symbols"
 *   also checks whether previous line contains "hidden" to skip it */
QList<XkbVariant> X11Tools::getVariants (const QString &layout, const QString &x11Dir) {
  QList<XkbVariant> result;
  //
  QString file = x11Dir+"xkb/symbols/";
  // workaround for XFree 4.3 new directory for one-group layouts
  if (QDir(file+"pc").exists()) file += "pc/";
  file += layout;
  qDebug() << "reading variants from " << file;
  //
  QFile f(file);
  //
  if (f.open(QIODevice::ReadOnly)) {
    QTextStream ts(&f);
    QString line;
    QString prev_line;
    //
    while (ts.status() == QTextStream::Ok) {
      QString str;
      int pos, pos2;
      XkbVariant variant;
      //
      prev_line = line;
      str = ts.readLine();
      if (str.isNull()) break;
      line = str.simplified();
      if (line.isEmpty() || line[0] == '#' || line.left(2) == "//") continue;
      pos = line.indexOf("xkb_symbols");
      if (pos < 0) continue;
      if (prev_line.indexOf("hidden") >= 0) continue;
      pos = line.indexOf('"', pos)+1;
      pos2 = line.indexOf('"', pos);
      if (pos < 0 || pos2 < 0) continue;
      variant.name = line.mid(pos, pos2-pos);
      variant.description = line.mid(pos, pos2-pos);
      result.append(variant);
      //qDebug() << "adding variant " << line.mid(pos, pos2-pos);
    }
    f.close();
  }
  return result;
}


/*
JrugrCfg X11Tools::groupNames (Display *dpy) {
  Atom real_prop_type;
  int fmt;
  unsigned long nitems, extra_bytes;
  char *prop_data = NULL;
  Status ret;
  JrugrCfg xkbConfig;
  Atom rules_atom = XInternAtom(dpy, _XKB_RF_NAMES_PROP_ATOM, False);
  //
  // no such atom!
  if (rules_atom == None) {
    // property cannot exist
    qCritical() << "X11Tools:Failed to fetch layouts from server:" << "could not find the atom" << _XKB_RF_NAMES_PROP_ATOM;
    return xkbConfig;
  }
  //
  ret = XGetWindowProperty(dpy, QX11Info::appRootWindow(), rules_atom, 0L, _XKB_RF_NAMES_PROP_MAXLEN, False, XA_STRING, &real_prop_type, &fmt, &nitems, &extra_bytes, (unsigned char **)(void *)&prop_data);
  // property not found!
  if (ret != Success) {
    qCritical() << "X11Tools:Failed to fetch layouts from server:" << "Could not get the property";
    return xkbConfig;
  }
  // has to be array of strings
  if (extra_bytes > 0 || real_prop_type != XA_STRING || fmt != 8) {
    if (prop_data) XFree(prop_data);
    qCritical() << "X11Tools:Failed to fetch layouts from server:" << "Wrong property format";
    return xkbConfig;
  }
  //
  qDebug() << "prop_data:" << nitems << prop_data;
  QStringList names;
  for (char *p = prop_data; p-prop_data < (long)nitems && p != NULL; p += strlen(p)+1) {
    qDebug() << " " << p;
    names.append(p);
  }
  //
  if (names.count() >= 4) {
    //{ rules, model, layouts, variants, options }
    xkbConfig.model = names[1];
    //qDebug() << "model:" << xkbConfig.model;
    QStringList layouts = names[2].split(OPTIONS_SEPARATOR);
    QStringList variants = names[3].split(OPTIONS_SEPARATOR);
    //
    for (int f = 0; f < layouts.count(); ++f) {
      LayoutUnit lu;
      //
      lu.layout = layouts[f];
      lu.variant = f < variants.count() ? variants[f] : "";
      xkbConfig.layouts << lu;
      qDebug() << "X11Tools:layout nm:" << lu.layout << "variant:" << lu.variant;
    }
    //
    if (names.count() >= 5) {
      QString options = names[4];
      xkbConfig.options = options.split(OPTIONS_SEPARATOR);
      qDebug() << "X11Tools:options:" << options;
    }
  }
  //
  XFree(prop_data);
  return xkbConfig;
}
*/


////////////////////////////////////////////////////////////////////////////////
QString X11Tools::atomName (Atom atom) {
  char *an;
  QString res;
  //
  if (atom == None) return res;
  an = XGetAtomName(QX11Info::display(), atom);
  if (an == NULL) return res;
  res = QString::fromLatin1(an);
  XFree(an);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
Window X11Tools::activeWindow () {
  Atom aType;
  int format;
  unsigned long itemCount;
  unsigned long bytesAfter;
  unsigned char *propRes = NULL;
  static Atom atomAW = None;
  int status;
  Window res = None;
  Display *dpy = QX11Info::display();
  //
  if (atomAW == None) {
    atomAW = XInternAtom(dpy, "_NET_ACTIVE_WINDOW", True);
    if (atomAW == None) return None;
  }
  status = XGetWindowProperty(dpy, XDefaultRootWindow(dpy), atomAW, 0, 1, False, AnyPropertyType, &aType, &format, &itemCount, &bytesAfter, &propRes);
  if (status >= Success) {
    if (propRes != NULL && itemCount > 0 && format == 32) res = *((Window *)propRes);
    if (propRes != NULL) XFree(propRes);
  }
  return res;
}


int X11Tools::activeDesktop () {
  Atom aType;
  int format;
  unsigned long itemCount;
  unsigned long bytesAfter;
  unsigned char *propRes = NULL;
  static Atom atomAD = None;
  int status;
  int res = 0;
  Display *dpy = QX11Info::display();
  //
  if (atomAD == None) {
    atomAD = XInternAtom(dpy, "_NET_CURRENT_DESKTOP", True);
    if (atomAD == None) return None;
  }
  status = XGetWindowProperty(dpy, XDefaultRootWindow(dpy), atomAD, 0, 1, False, AnyPropertyType, &aType, &format, &itemCount, &bytesAfter, &propRes);
  if (status >= Success) {
    if (propRes != NULL && itemCount > 0 && format == 32) res = *((int32_t *)propRes);
    if (propRes != NULL) XFree(propRes);
  }
  return res>=0?res:0;
}


Window X11Tools::windowTransientFor (Window w) {
  Atom aType;
  int format;
  unsigned long itemCount;
  unsigned long bytesAfter;
  unsigned char *propRes = NULL;
  static Atom atomWTF = None;
  int status;
  Window res = None;
  Display *dpy = QX11Info::display();
  //
  if (atomWTF == None) {
    atomWTF = XInternAtom(dpy, "WM_TRANSIENT_FOR", True);
    if (atomWTF == None) return None;
  }
  status = XGetWindowProperty(dpy, w, atomWTF, 0, 1, False, AnyPropertyType, &aType, &format, &itemCount, &bytesAfter, &propRes);
  if (status >= Success) {
    //qDebug() << "WM_TRANSIENT_FOR: format:" << format << "count:" << itemCount;
    if (propRes != NULL && itemCount > 0 && format == 32) res = *((Window *)propRes);
    if (propRes != NULL) XFree(propRes);
  }
  return res;
}


int X11Tools::windowDesktop (Window w) {
  Atom aType;
  int format;
  unsigned long itemCount;
  unsigned long bytesAfter;
  unsigned char *propRes = NULL;
  static Atom atomWTF = None;
  int status;
  int32_t res = 0;
  Display *dpy = QX11Info::display();
  //
  if (atomWTF == None) {
    atomWTF = XInternAtom(dpy, "_NET_WM_DESKTOP", True);
    if (atomWTF == None) return -1;
  }
  status = XGetWindowProperty(dpy, w, atomWTF, 0, 1, False, AnyPropertyType, &aType, &format, &itemCount, &bytesAfter, &propRes);
  if (status >= Success) {
    //qDebug() << "WM_TRANSIENT_FOR: format:" << format << "count:" << itemCount;
    if (propRes != NULL && itemCount > 0 && format == 32) res = *((int32_t *)propRes);
    if (propRes != NULL) XFree(propRes);
  }
  return res>=0?res:-1;
}


QString X11Tools::windowClass (Window w) {
  static Atom atomWC = None;
  //
  if (atomWC == None) atomWC = XInternAtom(QX11Info::display(), "WM_CLASS", True);
  return getStrProp(w, atomWC);
}


QString X11Tools::windowName (Window w) {
  static Atom atomNVN = None, atomNN = None, atomWN = None;
  QString res;
  //
  if (atomNVN == None) atomNVN = XInternAtom(QX11Info::display(), "_NET_WM_VISIBLE_NAME", True);
  res = getStrProp(w, atomNVN);
  if (!res.isNull()) return res;
  //
  if (atomNN == None) atomNN = XInternAtom(QX11Info::display(), "_NET_WM_NAME", True);
  res = getStrProp(w, atomNN);
  if (!res.isNull()) return res;
  //
  if (atomWN == None) atomWN = XInternAtom(QX11Info::display(), "WM_NAME", True);
  return getStrProp(w, atomWN);
}


pid_t X11Tools::windowPID (Window w) {
  Atom aType;
  int format;
  unsigned long itemCount;
  unsigned long bytesAfter;
  unsigned char *propPID = NULL;
  static Atom atomPID = None;
  int status;
  pid_t pid = 0;
  Display *dpy = QX11Info::display();
  //
  if (atomPID == None) {
    atomPID = XInternAtom(dpy, "_NET_WM_PID", False);
    if (atomPID == None) return 0;
  }
  status = XGetWindowProperty(dpy, w, atomPID, 0, 1, False, XA_CARDINAL, &aType, &format, &itemCount, &bytesAfter, &propPID);
  if (status >= Success) {
    if (propPID != NULL && itemCount > 0 && format == 32) pid = *((pid_t *)propPID);
    if (propPID != NULL) XFree(propPID);
  }
  return pid;
}


static QX11WindowList getWinList (Atom atomCL) {
  Atom aType;
  int format;
  unsigned long itemCount, bytesAfter;
  unsigned char *data = NULL;
  int status;
  Display *dpy = QX11Info::display();
  QX11WindowList res;
  //
  if (atomCL == None) return res;
  status = XGetWindowProperty(dpy, XDefaultRootWindow(dpy), atomCL, 0, 0x7fffffff, False, AnyPropertyType, &aType, &format, &itemCount, &bytesAfter, &data);
  if (status >= Success) {
    if (format == 32 && itemCount > 0) {
      Window *ww = (Window *)data;
      //
      for (unsigned int f = 0; f < itemCount; ++f, ++ww) if (*ww != None) res << (*ww);
    }
    if (data != NULL) XFree(data);
  }
  return res;
}


QX11WindowList X11Tools::topLevelWindows () {
  static Atom atomCL = None;
  //
  if (atomCL == None) {
    Display *dpy = QX11Info::display();
    //
    atomCL = XInternAtom(dpy, "_NET_CLIENT_LIST", False);
    if (atomCL == None) return QX11WindowList();
  }
  return getWinList(atomCL);
}


QX11WindowList X11Tools::topLevelWindowStack () {
  static Atom atomCL = None;
  //
  if (atomCL == None) {
    Display *dpy = QX11Info::display();
    //
    atomCL = XInternAtom(dpy, "_NET_CLIENT_LIST_STACKING", False);
    if (atomCL == None) return QX11WindowList();
  }
  return getWinList(atomCL);
}


////////////////////////////////////////////////////////////////////////////////
// (x,y) -- tray window position
// (w,h) -- desktop size
QRect X11Tools::trayPosition () {
  QPoint p;
  Display *dpy = QX11Info::display();
  QString nameTA;
  Atom atomST;
  Window tw;
  //
  nameTA.setNum(QX11Info::appScreen());
  nameTA.prepend("_NET_SYSTEM_TRAY_S");
  atomST = XInternAtom(dpy, nameTA.toLatin1().data(), False);
  tw = XGetSelectionOwner(dpy, atomST);
  //
  if (tw != None) {
    Window child, junkRoot;
    int x, y, width, height, trayX, trayY, trayW, trayH;
    unsigned int tmp0, tmp1;
    //
    XGetGeometry(dpy, QX11Info::appRootWindow(), &junkRoot, &x, &y, (unsigned int *)&trayW, (unsigned int *)&trayH, &tmp0, &tmp1);
    XGetGeometry(dpy, tw, &junkRoot, &x, &y, (unsigned int *)&width, (unsigned int *)&height, &tmp0, &tmp1);
    XTranslateCoordinates(dpy, tw, QX11Info::appRootWindow(), 0, 0, &trayX, &trayY, &child);
    qDebug("tray: trayX=%d; trayY=%d; trayW=%d; trayH=%d", trayX, trayY, trayW, trayH);
    return QRect(trayX, trayY, trayW, trayH);
  }
  //
  return QRect();
}


////////////////////////////////////////////////////////////////////////////////
void X11Tools::setActiveWindow (Window w) {
  static Atom atomRW = None;
  XEvent ev;
  Display *dpy = QX11Info::display();
  //
  if (w == None) return;
  //
  if (atomRW == None) {
    //atomRW = XInternAtom(dpy, "_NET_RESTACK_WINDOW", False);
    atomRW = XInternAtom(dpy, "_NET_ACTIVE_WINDOW", False);
    if (atomRW == None) return;
  }
  //
  memset(&ev, 0, sizeof(ev));
  ev.xclient.type = ClientMessage;
  ev.xclient.serial = 0;
  ev.xclient.send_event = True;
  ev.xclient.message_type = atomRW;
  ev.xclient.window = w;
  ev.xclient.format = 32;
  ev.xclient.data.l[0] = 2; // pretend to be a pager
  XSendEvent(dpy, XDefaultRootWindow(dpy), False, /*NoEventMask*/SubstructureRedirectMask|SubstructureNotifyMask, &ev);
  XFlush(dpy);
}


QX11AtomList X11Tools::windowState (Window w) {
  Atom aType;
  int format;
  unsigned long itemCount, bytesAfter;
  unsigned char *data = NULL;
  int status;
  static Atom atomWT = None;
  Display *dpy = QX11Info::display();
  QX11AtomList res;
  //
  if (atomWT == None) {
    atomWT = XInternAtom(dpy, "_NET_WM_STATE", True);
    if (atomWT == None) return res;
  }
  status = XGetWindowProperty(dpy, w, atomWT, 0, 0x7fffffff, False, AnyPropertyType, &aType, &format, &itemCount, &bytesAfter, &data);
  if (status >= Success) {
    if (format == 32 && itemCount > 0) {
      const Atom *a = (const Atom *)data;
      //
      for (unsigned int f = 0; f < itemCount; ++f, ++a) res << *a;
    }
    if (data != NULL) XFree(data);
  }
  return res;
}


QStringList X11Tools::windowStateNames (Window w) {
  Atom aType;
  int format;
  unsigned long itemCount, bytesAfter;
  unsigned char *data = NULL;
  int status;
  static Atom atomWT = None;
  Display *dpy = QX11Info::display();
  QStringList res;
  //
  if (atomWT == None) {
    atomWT = XInternAtom(dpy, "_NET_WM_STATE", True);
    if (atomWT == None) return res;
  }
  status = XGetWindowProperty(dpy, w, atomWT, 0, 0x7fffffff, False, AnyPropertyType, &aType, &format, &itemCount, &bytesAfter, &data);
  if (status >= Success) {
    if (format == 32 && itemCount > 0) {
      const Atom *a = (const Atom *)data;
      //
      for (unsigned int f = 0; f < itemCount; ++f, ++a) {
        char *name = XGetAtomName(dpy, *a);
        if (name != NULL && name[0]) {
          res << name;
          XFree(name);
        }
      }
    }
    if (data != NULL) XFree(data);
  }
  return res;
}


Window X11Tools::appWindow (Window w) {
  if (w == None) return None;
  for (;;) {
    Window p = windowTransientFor(w);
    if (p == None) return w;
    w = p;
  }
}


static Atom jpname (const QString &name) {
  QString n(name);
  //
  n.prepend("JRUGR_");
  return XInternAtom(QX11Info::display(), n.toUtf8().constData(), False);
}


static Atom jtname (void) {
  static Atom a = None;
  //
  if (a == None) a = XInternAtom(QX11Info::display(), "JSTRING", False);
  return a;
}


QString X11Tools::getjprop (Window w, const QString &name) {
  Atom aType;
  int format;
  unsigned long itemCount, bytesAfter;
  unsigned char *data = NULL;
  int status;
  QString res;
  //
  if (w == None) w = QX11Info::appRootWindow();
  status = XGetWindowProperty(QX11Info::display(), w, jpname(name), 0, 0x7fffffff, False, jtname(), &aType, &format, &itemCount, &bytesAfter, &data);
  if (status >= Success) {
    if (format == 8 && itemCount > 0) res = QString::fromUtf8((const char *)data, itemCount);
    if (data != NULL) XFree(data);
  }
  return res;
}


void X11Tools::setjprop (Window w, const QString &name, const QString &value) {
  QByteArray a(value.toUtf8());
  //
  if (w == None) w = QX11Info::appRootWindow();
  XChangeProperty(QX11Info::display(), w, jpname(name), jtname(), 8, PropModeReplace, (const unsigned char *)(a.constData()), a.size());
}


void X11Tools::deljprop (Window w, const QString &name) {
  if (w == None) w = QX11Info::appRootWindow();
  XDeleteProperty(QX11Info::display(), w, jpname(name));
}


////////////////////////////////////////////////////////////////////////////////
/**
 * @brief Gets the single layout part of a layout(variant) string
 * @param[in] layvar String in form layout(variant) to parse
 * @return The layout found in the string
 */
const QString LayoutUnit::parseLayout (const QString &layvar) {
  static const char *LAYOUT_PATTERN = "[a-zA-Z0-9_/-]*";
  QString varLine = layvar.trimmed();
  QRegExp rx(LAYOUT_PATTERN);
  int pos = rx.indexIn(varLine, 0);
  int len = rx.matchedLength();
  // check for errors
  if (pos < 0 || len < 2) return "";
  return varLine.mid(pos, len);
}


/**
 * @brief Gets the single variant part of a layout(variant) string
 * @param[in] layvar String in form layout(variant) to parse
 * @return The variant found in the string, no check is performed
 */
const QString LayoutUnit::parseVariant (const QString &layvar) {
  static const char* VARIANT_PATTERN = "\\([a-zA-Z0-9_-]*\\)";
  QString varLine = layvar.trimmed();
  QRegExp rx(VARIANT_PATTERN);
  int pos = rx.indexIn(varLine, 0);
  int len = rx.matchedLength();
  // check for errors
  if (pos < 2 || len < 2) return "";
  return varLine.mid(pos+1, len-2);
}
