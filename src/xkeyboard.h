/*************************************************************************
 * based on xkeyboard.h by Leonid Zeitlin (lz@europe.com)                *
 * heavily modified by Ketmar // Vampire Avalon                          *
 *                                                                       *
 * This program is free software; you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation; either version 2 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 *************************************************************************/
#ifndef JRUGRXKEYBOARD_H
#define JRUGRXKEYBOARD_H

#include "jrugrdefs.h"


typedef struct {
  QString name;
  QString sym;
} XkbLayoutInfo;


typedef QList<XkbLayoutInfo> QXkbLayoutList;


class XKeyboard : public QObject {
  Q_OBJECT
public:
  XKeyboard ();
  ~XKeyboard ();

  inline bool isXKBAvailable () { return mXKbAvailable; } // is XKEYBOARD extension available in the X server?

  int groupCount (); // return number of the currently configured keyboard groups
  void groupNames (QStringList &list); // get the names of the currently configured keyboard groups
  void symbolNames (QStringList &list); // get the 'short' names of the currently configured keyboard groups

  void groupInfo (QXkbLayoutList &list);

  void setActiveGroup (int groupno); // set the current keyboard group to the given groupno
  int activeGroup (); // return the current keyboard group index

  void processEvent (XEvent *ev);

  static int findBySym (const QXkbLayoutList &lst, const QString &sym);

signals:
  void groupChanged (int groupno); // signals that new keyboard group is selected
  void layoutChanged (); // signals that keyboard layout has changed and thus we need to reconfigure

private:
  int mEventCode;
  bool mXKbAvailable;
};


#endif
