////////////////////////////////////////
//  Copyright : GPL                   //
////////////////////////////////////////
#ifndef JRUGRX11TOOLS_H
#define JRUGRX11TOOLS_H

#include "jrugrdefs.h"

#include <sys/types.h>


struct XkbOption;
struct XkbVariant;
struct LayoutUnit;


struct XkbOptionGroup {
  QString name;
  QString description;
  bool exclusive;
  QList<XkbOption> options;
};


struct XkbOption {
  QString name;
  QString description;
  //XkbOptionGroup* group;
};


struct RulesInfo {
  QHash<QString, QString> models;
  QHash<QString, QString> layouts;
  QHash<QString, XkbOption> options;
  QHash<QString, XkbOptionGroup> optionGroups;
};


struct XkbVariant {
  QString name;
  QString description;
};


class LayoutUnit {
public:
  LayoutUnit () {}
  LayoutUnit (const QString &aLayout, const QString &aVariant) : layout(aLayout), variant(aVariant) {}
  LayoutUnit (const QString &pair) { setFromPair(pair); }

  inline void setDisplayName (const QString &name) { mDisplayName = name; }
  inline QString getDisplayName () const { return !mDisplayName.isEmpty()?mDisplayName:getDefaultDisplayName(layout, variant); }

  inline void setFromPair (const QString& pair) { layout = parseLayout(pair); variant = parseVariant(pair); }

  inline QString toPair () const {
    if (variant.isEmpty()) return layout;
    return QString("%1(%2)").arg(layout, variant);
  }

  inline bool operator != (const LayoutUnit &lu) const { return layout!=lu.layout || variant!=lu.variant; }
  inline bool operator == (const LayoutUnit &lu) const { return layout==lu.layout && variant==lu.variant; }

  static inline QString getDefaultDisplayName (const QString &layout, const QString &variant="") { Q_UNUSED(variant); return layout.left(3); }

  static const QString parseLayout (const QString &layvar);
  static const QString parseVariant (const QString &layvar);

public:
  QString layout;
  QString variant;

private:
  QString mDisplayName;
};


typedef QList<Window> QX11WindowList;
typedef QList<Atom> QX11AtomList;


class X11Tools {
public:
  X11Tools () {}

  static const QString findX11Dir ();
  static const QString findXkbRulesFile (const QString &x11Dir, Display *dpy);
  static QList<XkbVariant> getVariants (const QString &layout, const QString &x11Dir);
  static RulesInfo *loadRules (const QString &rulesFile, bool layoutsOnly=false);
  //static JrugrCfg groupNames (Display *dpy);

  static QString atomName (Atom atom);

  static Window activeWindow ();
  static int activeDesktop ();

  static void setActiveWindow (Window w);

  static Window windowTransientFor (Window w);
  static int windowDesktop (Window w); // -1: want to be 'sticky'

  static QString windowClass (Window w);
  static QString windowName (Window w);
  static pid_t windowPID (Window w); // 0: unknown

  static QX11WindowList topLevelWindows ();
  static QX11WindowList topLevelWindowStack ();

  static Window appWindow (Window w);

  static QRect trayPosition ();

  static QX11AtomList windowState (Window w);
  static QStringList windowStateNames (Window w);

  static QString getjprop (Window w, const QString &name);
  static void setjprop (Window w, const QString &name, const QString &value);
  static void deljprop (Window w, const QString &name);

private:
  static XkbOptionGroup createMissingGroup (const QString &groupName);
  static bool isGroupExclusive (const QString &groupName);
};


#endif
