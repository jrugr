////////////////////////////////////////
//  Copyright : GPL                   //
////////////////////////////////////////
#ifndef JRUGR_H
#define JRUGR_H

#define K8_MPLAYER_HACK
#define MPLAYER_HACK_TIMEOUT  (300)


#include "jrugrdefs.h"
#include "xkeyboard.h"
#include "x11tools.h"
#include "jcfg.h"
#include "jrugrconfui.h"

#include <QAbstractEventDispatcher>
#ifdef K8_MPLAYER_HACK
# include <QTimer>
#endif


typedef struct {
  Window w;
  Window appWindow;
  int layout; // language for this window
} WinInfo;


typedef QHash<Window, WinInfo *> QWIHash;
typedef QHashIterator<Window, WinInfo *> QWIHashIterator;
typedef QMutableHashIterator<Window, WinInfo *> QWIHashMutableIterator;


class Jrugr : public QApplication {
  Q_OBJECT

public:
  Jrugr (int &argc, char **argv);
  ~Jrugr ();

  bool firstStart ();
  void startup ();
  void initialize ();

signals:
  void changeLayout (int index);
  void activeWindowChanged ();
  void activeDesktopChanged ();
  void clientListChanged ();

public slots:
  void onLayoutChanged ();
  void onGroupChanged (int index);
  void setNextGroup ();
  void setPrevGroup ();
  void trayClicked (QSystemTrayIcon::ActivationReason reason);

protected slots:
  void reconfigure ();

private slots:
  void actionsActivate (QAction *action);
  void onChangeLayout (int index);

  void doActiveWindowChanged ();
  void doActiveDesktopChanged ();
  void onClientListChanged ();

#ifdef K8_MPLAYER_HACK
  void onMPFix ();
#endif

protected:
  virtual bool x11EventFilter (XEvent *);

  void mousePressEvent (QMouseEvent *);
  int setKeyLayout (QString keyConf);
  void configure ();
  void configureXkb ();

private:
  void buildIcon ();
  void createMenu ();
  bool loadRules ();

  QIcon langIcon (const QString &lsym, int wdt=32, int hgt=22, bool noFlag=false); // get icon for language defined by it's sym ("us", etc)

  void onActiveWindowChanged (Window w);
  void onActiveDesktopChanged (int desk);

  void installFacehuggers ();

  bool isKnownWindow (Window w) const;
  bool isKnownApp (Window w) const;

  WinInfo *findWindowInfo (Window w);
  WinInfo *findAppInfo (Window w); // find application info for the given window

  int desktopLayout (int desk) const;
  void setDesktopLayout (int desk, int group);

  Window deskActiveWindow (int desk);
  void setDeskActiveWindow (int desk, Window w);

  void windowDies (Window w);

#ifdef K8_MPLAYER_HACK
  bool deactivateMPlayer ();
#endif

  int layIndex (Window w, const QString &propname, bool unknownIsZero=false) const;

private:
  QAbstractEventDispatcher *mEDisp;
  //
  QString mLangThemePath;
  QXkbLayoutList mGroupInfo; // list of all active groups (those that can be activated)
  int mActiveGroup; // currently active group
  //
  XKeyboard *mXkb;
  QSystemTrayIcon *mTrayIcon;
  QMenu *mTrayMenu;
  //
  JrugrCfg *jCfg;
  //
  QWIHash mKnownWindows;
  QWIHash mKnownApps;
  QList<int> mDeskLangs;
  QList<Window> mDeskActiveWindows;
  //
  int mCurDesk;
  Window mActiveWindow;
  //
  bool mDeskJustChanged;
  //
  QString mCfgFileName;
  //
#ifdef K8_MPLAYER_HACK
  bool mMplayerHack;
  QTimer *mMPFix;
#endif
};


#endif
