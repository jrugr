////////////////////////////////////////
//  Copyright : GPL                   //
////////////////////////////////////////
#include "jrugrdefs.h"
#include "jrugr.h"
#include "xkeyboard.h"


static void fixTextCodec () {
  QTextCodec *kc;
#ifdef WIN32
  kc = QTextCodec::codecForName("windows-1251");
  if (!kc) return;
  QTextCodec::setCodecForCStrings(kc);
  QTextCodec::setCodecForLocale(kc);
#else
  const char *ll = getenv("LANG");
  if (!ll || !ll[0]) ll = getenv("LC_CTYPE");
  if (ll && ll[0] && (strcasestr(ll, "utf-8") || strcasestr(ll, "utf8"))) return;
  kc = QTextCodec::codecForName("koi8-r");
  if (!kc) return;
  QTextCodec::setCodecForCStrings(kc);
  QTextCodec::setCodecForLocale(kc);
#endif
}


#ifdef DEBUG_OUTPUT
static bool debugOutput = true;
#else
static bool debugOutput = false;
#endif

static void myMessageOutput (QtMsgType type, const char *msg)  {
  if (debugOutput) {
    switch (type) {
      case QtDebugMsg: fprintf(stderr, "Debug: %s\n", msg); break;
      case QtWarningMsg: fprintf(stderr, "Warning: %s\n", msg); break;
      case QtCriticalMsg: fprintf(stderr, "Critical: %s\n", msg); break;
      case QtFatalMsg: fprintf(stderr, "Fatal: %s\n", msg); abort();
    }
  } else {
    if (type == QtFatalMsg) { fprintf(stderr, "Fatal: %s\n", msg); abort(); }
  }
}


static void setDebugOutput (int argc, char *argv[]) {
  qInstallMsgHandler(myMessageOutput);
  //
  for (int f = 1; f < argc; ++f) {
    if (strcmp(argv[f], "-goobers") == 0) {
      debugOutput = 1;
      for (int c = f; c < argc; ++c) argv[c] = argv[c+1];
      argv[--argc] = NULL;
      --f;
    }
  }
}


int main (int argc, char *argv[]) {
  setDebugOutput(argc, argv);
  //
  Jrugr app(argc, argv);

  fixTextCodec();

  QTranslator translator;
  qDebug() << "Locale:" << QLocale::system().name();
  QString lang = "jrugr_" + QLocale::system().name().split("_")[0];
  qDebug()<<"Language:"<<lang;

#if 0
  QString langPath(JRUGR_TRANSLATION_PATH);
  qDebug()<<"Language path (trying):"<<langPath;
  if (!QFile::exists(langPath+"/"+lang+".qm")) langPath =  QCoreApplication::applicationDirPath() + "/language/";
  qDebug()<<"Language path (fallback):"<<langPath;
  translator.load(lang, langPath);
  app.installTranslator(&translator);
#else
  QString langPath(QCoreApplication::applicationDirPath() + "/language/");
  translator.load(lang, langPath);
  app.installTranslator(&translator);
#endif

  app.startup();
  app.initialize();
  return app.exec();
}
