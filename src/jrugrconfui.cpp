////////////////////////////////////////
//  Copyright : GPL                   //
////////////////////////////////////////
#include "jrugrconfui.h"


JrugrConfigForm::JrugrConfigForm (const QString &optFileName, QWidget *parent) : QDialog(parent) {
  setAttribute(Qt::WA_DeleteOnClose);
  //
  mCfgFileName = optFileName;
  cfgUI.setupUi(this);
  // initialize the binding between list and stackedWidget
  cfgUI.listWidget->setCurrentRow(0);
  //
  {
    QSettings settings(mCfgFileName, QSettings::IniFormat, this);
    settings.beginGroup("Style");
    mTheme = settings.value("path").toString();
    mIcoPath = mTheme+"/language/";
    settings.endGroup(); //Style
  }
  qDebug() << "Theme " << mTheme;
  qDebug() << "Icons " << mIcoPath;
  //
  mJCfg = JrugrCfg::load(mCfgFileName);
  //
  switch (mJCfg->switching) {
    case GLOBAL_LAYOUT: cfgUI.rbPolicyGlobal->setChecked(true); break;
    case APP_LAYOUT: cfgUI.rbPolicyApp->setChecked(true); break;
    case WIN_LAYOUT: cfgUI.rbPolicyWin->setChecked(true); break;
    case DESK_LAYOUT: cfgUI.rbPolicyDesk->setChecked(true); break;
  }
  //
  connect(cfgUI.chkMPHack, SIGNAL(clicked()), SLOT(setMPHack()));
  cfgUI.chkMPHack->setChecked(mJCfg->mplayerHack);
  //
  connect(cfgUI.buttonBox, SIGNAL(rejected()), this, SLOT(close()));
  connect(cfgUI.buttonBox, SIGNAL(accepted()), SLOT(apply()));
  connect(cfgUI.rbUseXkb, SIGNAL(clicked(bool)), SLOT(statSelect(bool)));
  connect(cfgUI.rbIndicator, SIGNAL(clicked(bool)), SLOT(statSelect(bool)));
  connect(cfgUI.rbNoXkb, SIGNAL(clicked(bool)), SLOT(statSelect(bool)));
  //
  connect(cfgUI.rbPolicyGlobal, SIGNAL(clicked(bool)), SLOT(statSwitching(bool)));
  connect(cfgUI.rbPolicyApp, SIGNAL(clicked(bool)), SLOT(statSwitching(bool)));
  connect(cfgUI.rbPolicyWin, SIGNAL(clicked(bool)), SLOT(statSwitching(bool)));
  connect(cfgUI.rbPolicyDesk, SIGNAL(clicked(bool)), SLOT(statSwitching(bool)));
  //
  cfgUI.btnAdd->setEnabled(false);
  cfgUI.btnAdd->setIcon(QIcon::fromTheme(QString("arrow-right"), QIcon(mTheme+"/add.png")));
  //
  cfgUI.btnRemove->setEnabled(false);
  cfgUI.btnRemove->setIcon(QIcon::fromTheme(QString("arrow-left"), QIcon(mTheme+"/rem.png")));
  //
  cfgUI.btnUp->setEnabled(false);
  cfgUI.btnUp->setIcon(QIcon::fromTheme(QString("arrow-up"), QIcon(mTheme+"/up.png")));
  //
  cfgUI.btnDown->setEnabled(false);
  cfgUI.btnDown->setIcon(QIcon::fromTheme(QString("arrow-down"), QIcon(mTheme+"/down.png")));
  //
  if (!setStat()) return;
  initXKBTab();
}


JrugrConfigForm::~JrugrConfigForm () {
  delete mJCfg;
}


void JrugrConfigForm::apply () {
  mJCfg->save(mCfgFileName);
  //
  if (cfgUI.stackedWidget->currentIndex() == 0 && mJCfg->workMode == USE_XKB) {
    QStringList parm = cfgUI.editCmdLine->text().split(" ");
    int qpres;
    //
    parm.removeAt(0);
    qDebug() << "parm:" << parm;
    qpres = QProcess::execute("setxkbmap", parm);
    qDebug() << "Set XKB result" << qpres;
  } else if (cfgUI.stackedWidget->currentIndex() == 2 && mJCfg->workMode == USE_XKB) {
    QStringList parm = cfgUI.editCmdLineOpt->text().split(" ");
    int qpres;
    //
    parm.removeAt(0);
    qDebug() << "parm:" << parm;
    qpres = QProcess::execute("setxkbmap", parm);
    qDebug() << "Set XKB result" << qpres;
  }
  //qDebug()<<"Close";
  //close();
  emit saveConfig();
}


void JrugrConfigForm::statSelect (bool check) {
  Q_UNUSED(check)
  //
  if (cfgUI.rbUseXkb->isChecked()) mJCfg->workMode = USE_XKB;
  if (cfgUI.rbIndicator->isChecked()) mJCfg->workMode = ONLY_INDICATION;
  if (!setStat()) return;
  initXKBTab();
}


void JrugrConfigForm::initXKBTab () {
  connect(cfgUI.btnAdd, SIGNAL(clicked()), SLOT(addLayout()));
  connect(cfgUI.btnRemove, SIGNAL(clicked()), SLOT(delLayout()));
  loadRules();
  //set comboModel list
  QHashIterator<QString, QString> i(mCurRule->models);
  while (i.hasNext()) { i.next(); mModelList << i.value(); }
  // set avaleble language
  mSrcLayoutModel = new SrcLayoutModel(mCurRule, mIcoPath, this);
  qDebug() << mIcoPath;
  mDstLayoutModel = new DstLayoutModel(mCurRule, mJCfg, mIcoPath, this);
  mXkbOptionsModel = new XkbOptionsModel(mCurRule, mJCfg, this);
  cfgUI.comboModel->addItems(mModelList);
  cfgUI.comboModel->setCurrentIndex(mModelList.indexOf(mCurRule->models.value(mJCfg->model)));
  cfgUI.srcTableView->setModel(mSrcLayoutModel);
  cfgUI.srcTableView->resizeColumnToContents(0);
  cfgUI.dstTableView->setModel(mDstLayoutModel);
  cfgUI.dstTableView->resizeColumnToContents(0);
  cfgUI.xkbOptionsTreeView->setModel(mXkbOptionsModel);
  cfgUI.xkbOptionsTreeView->header()->hide();
  cfgUI.xkbOptionsTreeView->expandAll();
  //
  connect(cfgUI.srcTableView, SIGNAL(clicked(const QModelIndex &)), SLOT(srcClick(QModelIndex)));
  connect(cfgUI.dstTableView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)), SLOT(dstClick()));
  connect(cfgUI.comboModel, SIGNAL(currentIndexChanged (int)) , SLOT(comboModelCh(int)));
  connect(cfgUI.comboVariant, SIGNAL(currentIndexChanged (int)), SLOT(comboVariantCh(int)));
  connect(cfgUI.btnUp, SIGNAL(clicked()), SLOT(layoutUp()));
  connect(cfgUI.btnDown, SIGNAL(clicked()), SLOT(layoutDown()));
  connect(mXkbOptionsModel, SIGNAL(dataChanged(const QModelIndex &, const QModelIndex &)), SLOT(xkbOptionsChanged(const QModelIndex &, const QModelIndex &)));
  //
  setCmdLine();
  updateOptionsCommand();
}


bool JrugrConfigForm::setStat () {
  connect(cfgUI.chkShowFlag, SIGNAL(clicked()), SLOT(setFlagUse()));
  //
  switch (mJCfg->workMode) {
    case ONLY_INDICATION:
      cfgUI.grpIndicatorOptions->setEnabled(true);
      cfgUI.grpLayouts->setEnabled(false);
      cfgUI.pgPolicy->setEnabled(true);
      cfgUI.pgXkb->setEnabled(false);
      cfgUI.rbUseXkb->setChecked(false);
      cfgUI.rbIndicator->setChecked(true);
      cfgUI.rbNoXkb->setChecked(false);
      return false;
    case USE_XKB:
      cfgUI.grpIndicatorOptions->setEnabled(true);
      cfgUI.grpLayouts->setEnabled(true);
      cfgUI.pgPolicy->setEnabled(true);
      cfgUI.pgXkb->setEnabled(true);
      cfgUI.rbUseXkb->setChecked(true);
      cfgUI.rbIndicator->setChecked(false);
      cfgUI.rbNoXkb->setChecked(false);
      return true;
  }
  return false;
}


void JrugrConfigForm::setCmdLine () {
  QString cmdLine = "setxkbmap -model "+mJCfg->model;
  //
  cmdLine += " -layout ";
  for (int f = 0;f < mJCfg->layouts.size(); ++f) {
    cmdLine += mJCfg->layouts[f].layout;
    if (f < mJCfg->layouts.size()-1) cmdLine += ',';
  }
  //
  cmdLine += " -variant ";
  for (int f = 0; f < mJCfg->layouts.size(); ++f) {
    cmdLine += mJCfg->layouts[f].variant;
    if (f < mJCfg->layouts.size()-1) cmdLine += ',';
  }
  cfgUI.editCmdLine->setText(cmdLine);
}


void JrugrConfigForm::addLayout () {
  LayoutUnit lu;
  QItemSelectionModel *selectionModel = cfgUI.srcTableView->selectionModel();
  //
  if (selectionModel == NULL || !selectionModel->hasSelection() || mJCfg->layouts.size() >= 4) return;
  QModelIndexList selected = selectionModel->selectedRows();
  qDebug() << "selected : " << selected;
  QString layout = mSrcLayoutModel->getLayoutAt(selected[0].row());
  if (layout.isEmpty() || mJCfg->layouts.contains(layout)) return;
  lu.layout = layout;
  mJCfg->layouts.append(lu);
  mDstLayoutModel->reset();
  cfgUI.dstTableView->update();
  setCmdLine();
}


void JrugrConfigForm::delLayout () {
  QItemSelectionModel *selectionModel = cfgUI.dstTableView->selectionModel();
  if (selectionModel == NULL || !selectionModel->hasSelection()|| mJCfg->layouts.size() == 0) return;
  QModelIndexList selected = selectionModel->selectedRows();
  mJCfg->layouts.removeAt(selected[0].row());
  mDstLayoutModel->reset();
  cfgUI.dstTableView->update();
  setCmdLine();
}


void JrugrConfigForm::srcClick (QModelIndex index) {
  Q_UNUSED(index)
  cfgUI.btnAdd->setEnabled(true);
}


void JrugrConfigForm::dstClick () {
  int row = getSelectedDstLayout();
  cfgUI.btnRemove->setEnabled(row!=-1);
  int el_count = cfgUI.comboVariant->count();
  for (int f = el_count; f > 0; --f) cfgUI.comboVariant->removeItem(f);
  //cfgUI.comboVariant->clear(); some sheet seg fail in use
  cfgUI.comboVariant->setEnabled(row!=-1);
  cfgUI.btnUp->setEnabled(row!=-1);
  cfgUI.btnDown->setEnabled(row!=-1);
  if (row == -1) return;
  QString layout = mJCfg->layouts[row].layout;
  qDebug() << layout;
  mVariants = X11Tools::getVariants(layout, X11Tools::findX11Dir());
  qDebug() << "JrugrConfigForm:addItem";
  if (cfgUI.comboVariant->count() == 0) {
    cfgUI.comboVariant->addItem(tr("Default"), "Default");
    cfgUI.comboVariant->setItemData(0, tr("Default"), Qt::ToolTipRole);
    cfgUI.comboVariant->setItemText(0, tr("Default"));
  }
  if (mVariants.count() > 0) {
    for (int f = 0; f < mVariants.count(); ++f) {
      cfgUI.comboVariant->addItem(mVariants[f].description, mVariants[f].name);
      cfgUI.comboVariant->setItemData(cfgUI.comboVariant->count()-1, mVariants[f].description, Qt::ToolTipRole);
    }
    QString variant = mJCfg->layouts[row].variant;
    if (variant != NULL && !variant.isEmpty()) {
      int idx = cfgUI.comboVariant->findData(variant);
      cfgUI.comboVariant->setCurrentIndex(idx);
    } else {
      cfgUI.comboVariant->setCurrentIndex(0);
    }
  }
}


bool JrugrConfigForm::loadRules () {
  QString x11dir = X11Tools::findX11Dir();
  if (x11dir.isNull() || x11dir.isEmpty()) return false;
  QString rulesFile = X11Tools::findXkbRulesFile(x11dir, QX11Info::display());
  if (rulesFile.isNull() || rulesFile.isEmpty()) return false;
  mCurRule = X11Tools::loadRules(rulesFile, false);
  if (mCurRule == NULL) return false;
  return true;
}


void JrugrConfigForm::comboModelCh (int index) {
  mJCfg->model= mCurRule->models.key(cfgUI.comboModel->itemText(index));
  setCmdLine();
}


void JrugrConfigForm::comboVariantCh (int index) {
  if (index == 0) mJCfg->layouts[getSelectedDstLayout()].variant = "";
  else if (index < mVariants.count()) mJCfg->layouts[getSelectedDstLayout()].variant = mVariants.at(index-1).name; // it sometimes crashed here with "index out of bounds"
  setCmdLine();
}


int JrugrConfigForm::getSelectedDstLayout () {
  QItemSelectionModel *selectionModel = cfgUI.dstTableView->selectionModel();
  if (selectionModel == NULL || !selectionModel->hasSelection()) return -1;
  QModelIndexList selected = selectionModel->selectedRows();
  int row = selected.count() > 0 ? selected[0].row() : -1;
  return row;
}


void JrugrConfigForm::setFlagUse () {
  mJCfg->showFlag = cfgUI.chkShowFlag->isChecked();
}


void JrugrConfigForm::setMPHack () {
  mJCfg->mplayerHack = cfgUI.chkMPHack->isChecked();
}


void JrugrConfigForm::layoutUp () {
  int row = getSelectedDstLayout();
  if (row>0) {
    LayoutUnit lu = mJCfg->layouts[row-1];
    mJCfg->layouts[row-1] = mJCfg->layouts[row];
    mJCfg->layouts[row] = lu;
    mDstLayoutModel->reset();
    cfgUI.dstTableView->update();
    setCmdLine();
  }
}


void JrugrConfigForm::layoutDown () {
  int row = getSelectedDstLayout();
  if (row < mJCfg->layouts.count()-1 && row > -1) {
    LayoutUnit lu = mJCfg->layouts[row+1];
    mJCfg->layouts[row+1] = mJCfg->layouts[row];
    mJCfg->layouts[row] = lu;
    mDstLayoutModel->reset();
    setCmdLine();
  }
}


void JrugrConfigForm::updateOptionsCommand () {
  QString cmd = "setxkbmap";
  if (cfgUI.chkResetOld->isChecked()) cmd += " -option";
  if (!mJCfg->options.empty()) {
    cmd += " -option ";
    cmd += mJCfg->options.join(",");
  }
  cfgUI.editCmdLineOpt->setText(cmd);
}


void JrugrConfigForm::xkbOptionsChanged (const QModelIndex &topLeft, const QModelIndex &bottomRight) {
  Q_UNUSED(topLeft)
  Q_UNUSED(bottomRight)
  updateOptionsCommand();
}


void JrugrConfigForm::closeEvent (QCloseEvent * /*event*/)  {
/*
  hide();
  event->ignore();
*/
}


void JrugrConfigForm::statSwitching (bool checked) {
  //Q_UNUSED(checked)
  qDebug() << "statSwitching";
  if (checked) {
    QRadioButton *radio = qobject_cast<QRadioButton *>(QObject::sender());
    if (radio == cfgUI.rbPolicyGlobal) mJCfg->switching = GLOBAL_LAYOUT;
    if (radio == cfgUI.rbPolicyApp) mJCfg->switching = APP_LAYOUT;
    if (radio == cfgUI.rbPolicyWin) mJCfg->switching = WIN_LAYOUT;
    if (radio == cfgUI.rbPolicyDesk) mJCfg->switching = DESK_LAYOUT;
    qDebug("  %d", mJCfg->switching);
  }
  /*
  if (cfgUI.rbPolicyGlobal->isChecked()) mJCfg->switching = GLOBAL_LAYOUT;
  if (cfgUI.rbPolicyApp->isChecked()) mJCfg->switching = APP_LAYOUT;
  if (cfgUI.rbPolicyWin->isChecked()) mJCfg->switching = WIN_LAYOUT;
  if (cfgUI.rbPolicyDesk->isChecked()) mJCfg->switching = DESK_LAYOUT;
  */
}


void JrugrConfigForm::getHotKeys (XEvent *event) {
  XKeyEvent *keys = (XKeyEvent *)event;
  //
  switch (XKeycodeToKeysym(QX11Info::display(), keys->keycode, 0)) {
    case XK_Shift_L:
    case XK_Shift_R:
      mMods = "Shift";
      mKey.clear();
      break;
    case XK_Control_L:
    case XK_Control_R:
      mMods = "Ctrl";
      mKey.clear();
      break;
    case XK_Alt_L:
    case XK_Alt_R:
      mMods = "Alt";
      mKey.clear();
      break;
    default:
      mKey = QString(XKeysymToString(XKeycodeToKeysym(QX11Info::display(), keys->keycode, 0)));
      if (mKey.count() < 2) mKey = mKey.toUpper();
  }
  if (!mMods.isEmpty()) mHotKeys =mMods+"+"+mKey;
  else if (mKey.count() > 1) mHotKeys = mKey;
}


void JrugrConfigForm::clearHotKeys () {
  qDebug() << "JrugrConfigForm:: Current mMods " << mMods;
  if (mMods.isEmpty()) return;
  qDebug() << "JrugrConfigForm:: Current mKey " << mKey;
  if (!mKey.isEmpty()) return;
  mMods.clear();
}
