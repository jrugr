////////////////////////////////////////
//  Copyright : GPL                   //
////////////////////////////////////////
#ifndef JRUGCONFIGS_H
#define JRUGCONFIGS_H

#include "jrugrdefs.h"
#include "x11tools.h"

#include <sys/types.h>


enum JWorkMode {
  ONLY_INDICATION,
  USE_XKB
};


enum JSwitchMode {
  GLOBAL_LAYOUT,
  APP_LAYOUT,
  WIN_LAYOUT,
  DESK_LAYOUT
};


class JrugrCfg {
public:
  JrugrCfg () : workMode(ONLY_INDICATION), switching(GLOBAL_LAYOUT), showFlag(true), useSU(true) {}

  static JrugrCfg *load (const QString &fname);
  bool save (const QString &fname) const;

public:
  QString model;
  QList<LayoutUnit> layouts;
  QStringList options;
  JWorkMode workMode;
  JSwitchMode switching;
  bool showFlag;
  bool useSU;
  bool mplayerHack;
};


#endif
