/*************************************************************************
 * based on xkeyboard.h by Leonid Zeitlin (lz@europe.com)                *
 * heavily modified by Ketmar // Vampire Avalon                          *
 *                                                                       *
 * This program is free software; you can redistribute it and/or modify  *
 * it under the terms of the GNU General Public License as published by  *
 * the Free Software Foundation; either version 2 of the License, or     *
 * (at your option) any later version.                                   *
 *                                                                       *
 *************************************************************************/
#include "xkeyboard.h"

//XkbNumKbdGroups -- maximum number of groups (currently 4)


XKeyboard::XKeyboard () {
  Display *dpy = QX11Info::display();
  int opcode, errorBase, major = XkbMajorVersion, minor = XkbMinorVersion;
  //
  // check the library version
  if (!XkbLibraryVersion(&major, &minor)) {
    qWarning() << QString(
      "This program was built against XKB extension library\n"
      "version %1.%2, but is run with the library version %3.%4.\n"
      "This may cause various problems and even result in a complete\n"
      "failure to function\n").arg(XkbMajorVersion).arg(XkbMinorVersion).arg(major).arg(minor);
  }

  // initialize the extension
  mXKbAvailable = XkbQueryExtension(dpy, &opcode, &mEventCode, &errorBase, &major, &minor);
  if (!mXKbAvailable) {
    qCritical() <<
      "The X Server does not support a compatible XKB extension.\n"
      "Either the server is not XKB-capable or the extension was disabled.\n"
      "This program would not work with this server, so it will exit now\n";
  } else {
    // register for XKB events
    // group state change, i.e. the current group changed:
    XkbSelectEventDetails(dpy, XkbUseCoreKbd, XkbStateNotify, XkbAllStateComponentsMask, XkbGroupStateMask);
    // keyboard mapping change:
    XkbSelectEventDetails(dpy, XkbUseCoreKbd, XkbMapNotify, XkbAllMapComponentsMask, XkbKeySymsMask);
    // group names change:
    XkbSelectEventDetails(dpy, XkbUseCoreKbd, XkbNamesNotify, XkbAllNamesMask, XkbGroupNamesMask);
    // new keyboard:
    XkbSelectEventDetails(dpy, XkbUseCoreKbd, XkbNewKeyboardNotify, XkbAllNewKeyboardEventsMask, XkbAllNewKeyboardEventsMask);
  }
}


XKeyboard::~XKeyboard () {
}


void XKeyboard::setActiveGroup (int groupno) {
  XkbStateRec state;
  //
  XkbLockGroup(QX11Info::display(), XkbUseCoreKbd, groupno);
  // the following call is necessary
  memset(&state, 0, sizeof(state));
  XkbGetState(QX11Info::display(), XkbUseCoreKbd, &state);
}


int XKeyboard::activeGroup () {
  XkbStateRec state;
  //
  memset(&state, 0, sizeof(state));
  XkbGetState(QX11Info::display(), XkbUseCoreKbd, &state);
  return (int)state.group;
}


int XKeyboard::groupCount () {
  XkbDescRec kbd;
  int res;
  //
  memset(&kbd, 0, sizeof(kbd));
  kbd.device_spec = XkbUseCoreKbd;
  XkbGetControls(QX11Info::display(), XkbGroupsWrapMask, &kbd);
  res = (int)kbd.ctrls->num_groups;
  XkbFreeControls(&kbd, XkbGroupsWrapMask, 1);
  return res;
}


extern "C" {
  static int IgnoreXError(Display *, XErrorEvent *) { return 0; }
}


void XKeyboard::groupNames (QStringList &list) {
  XErrorHandler oh;
  XkbDescRec kbd;
  char **names;
  int count;
  Display *dpy = QX11Info::display();
  //
  memset(&kbd, 0, sizeof(kbd));
  kbd.device_spec = XkbUseCoreKbd;
  //
  XkbGetControls(dpy, XkbGroupsWrapMask, &kbd);
  count = (int)kbd.ctrls->num_groups;
  XkbFreeControls(&kbd, XkbGroupsWrapMask, True);
  //
  names = (char **)calloc(count+1, sizeof(char *));
  XkbGetNames(dpy, XkbGroupNamesMask, &kbd);
  // XGetAtomNames below may generate BadAtom error, which is not a problem.
  // (it may happen if the name for a group was not defined)
  // Thus we temporarily ignore X errors
  oh = XSetErrorHandler(IgnoreXError);
  //
  XGetAtomNames(dpy, kbd.names->groups, count, names);
  // resume normal X error processing
  XSetErrorHandler(oh);
  for (int f = 0; f < count; ++f) {
    if (names[f]) {
      list.append(names[f]);
      XFree(names[f]);
    } else {
      list.append(QString::null);
    }
  }
  free(names);
  XkbFreeNames(&kbd, XkbGroupNamesMask, 1);
}


// pc+us+ru(winkey):2+inet(evdev)+group(menu_toggle)
void XKeyboard::symbolNames (QStringList &list) {
  XErrorHandler oh;
  XkbDescRec kbd;
  Atom symNameA;
  Display *dpy = QX11Info::display();
  //
  memset(&kbd, 0, sizeof(kbd));
  kbd.device_spec = XkbUseCoreKbd;
  //
  XkbGetNames(dpy, XkbSymbolsNameMask, &kbd);
  //
  symNameA = kbd.names->symbols;
  if (symNameA != None) {
    char *symName;
    //
    oh = XSetErrorHandler(IgnoreXError);
    symName = XGetAtomName(dpy, symNameA);
    // resume normal X error processing
    XSetErrorHandler(oh);
    //
    if (symName) {
      char *s;
      //
      //qDebug() << "symName:" << symName;
      // parse it
      s = strdup(symName);
      XFree(symName);
      symName = s;
      //
      s = symName;
      qDebug("symName: [%s]", s);
      while (*s) {
        char *e = strchr(s, '+'), ch, *t;
        //
        if (e == NULL) e = s+strlen(s);
        ch = *e; *e = '\0';
        if ((t = strchr(s, '(')) != NULL) *t = '\0';
        if ((t = strchr(s, ':')) != NULL) *t = '\0';
        if (strcasecmp(s, "pc") != 0 && strlen(s) == 2) {
          qDebug(" found: [%s]", s);
          list << s;
        }
        if (ch) *e++ = ch;
        s = e;
      }
      free(symName);
    }
  }
}


void XKeyboard::groupInfo (QXkbLayoutList &list) {
  QStringList gn, sn;
  //
  list.clear();
  groupNames(gn);
  symbolNames(sn);
  if (gn.size() < 1) {
    gn << "USA";
    if (sn.size() < 1) sn << "us";
  }
  for (int f = 0; f < gn.size(); ++f) {
    XkbLayoutInfo i;
    //
    i.name = gn[f];
    if (f < sn.size()) i.sym = sn[f]; else i.sym = "us";
    list << i;
  }
}


void XKeyboard::processEvent (XEvent *ev) {
  // XKB event?
  if (ev->type == mEventCode) {
    XkbEvent *xkbEv = (XkbEvent *)ev;
    //
    if (xkbEv->any.xkb_type == XkbStateNotify) {
      // state notify event, the current group has changed
      emit groupChanged(xkbEv->state.group);
    } else if ((xkbEv->any.xkb_type == XkbMapNotify && (xkbEv->map.changed & XkbKeySymsMask)) ||
               (xkbEv->any.xkb_type == XkbNamesNotify && (xkbEv->names.changed & XkbGroupNamesMask)) ||
               (xkbEv->any.xkb_type == XkbNewKeyboardNotify)) {
      // keyboard layout has changed
      emit layoutChanged();
    }
  }
}


int XKeyboard::findBySym (const QXkbLayoutList &lst, const QString &sym) {
  for (int f = 0; f < lst.size(); ++f) if (sym.compare(lst[f].sym, Qt::CaseInsensitive) == 0) return (int)f;
  return -1;
}
