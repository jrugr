////////////////////////////////////////
//  Copyright : GPL                   //
////////////////////////////////////////
#ifndef JRUGRDEFS_H
#define JRUGRDEFS_H

#include <QApplication>
#include <QPushButton>
#include <QMap>
#include <QLabel>
#include <QPainter>
#include <QPalette>
#include <QCheckBox>
#include <QVariant>
#include <QCursor>
#include <QTextStream>
#include <QFile>
#include <QFileInfo>
#include <QColor>
#include <QAbstractButton>
#include <QPixmap>
#include <QMouseEvent>
#include <QResizeEvent>
#include <QHash>
#include <QSizePolicy>
#include <QAction>
#include <QVariant>
#include <QMenu>
#include <QIcon>
#include <QWidget>
#include <QSpinBox>
#include <QByteArray>
#include <QFrame>
#include <QX11Info>
#include <QDesktopWidget>
#include <QDebug>
#include <QProcess>
#include <QDir>
#include <QDirIterator>
#include <QSettings>
#include <QTreeView>
#include <QMessageBox>
#include <QStringList>
#include <QListWidget>
#include <QTreeWidget>
#include <QSignalMapper>
#include <QFileDialog>
#include <QColorDialog>
#include <QGroupBox>
#include <QSpacerItem>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QStyleFactory>
#include <QToolBox>
#include <QComboBox>
#include <QRadioButton>
#include <QX11EmbedContainer>
#include <QX11EmbedWidget>
#include <QKeyEvent>
#include <QFileIconProvider>
#include <QCompleter>
#include <QTranslator>
#include <QLibraryInfo>
#include <QTextCodec>
#include <QClipboard>
#include <QStackedWidget>
#include <qwindowdefs.h>
#include <QHeaderView>
#include <QProgressBar>
#include <QSystemTrayIcon>
#include <QThread>
#include <QKeySequence>
#include <QSvgRenderer>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <X11/XKBlib.h>
#include <X11/extensions/XKBrules.h>


static inline QString fixLayoutName (const QString &s, bool useSU=false) {
  if (useSU && s == "ru") return QString("su");
  return s;
}


#endif
