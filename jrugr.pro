TEMPLATE = app
TARGET = jrugr

QT -= xml network
QT += gui
QT += svg


CONFIG += qt warn_on
CONFIG += debug_and_release
#CONFIG += debug
#CONFIG += release

#QMAKE_CFLAGS_RELEASE ~= s/\-O./-Os
#QMAKE_CXXFLAGS_RELEASE ~= s/\-O./-Os

##QMAKE_CFLAGS_RELEASE ~= s/\-O./-O2
##QMAKE_CXXFLAGS_RELEASE ~= s/\-O./-O2

QMAKE_CFLAGS_RELEASE += -march=native
QMAKE_CXXFLAGS_RELEASE += -march=native
QMAKE_CFLAGS_RELEASE += -mtune=native
QMAKE_CXXFLAGS_RELEASE += -mtune=native

QMAKE_LFLAGS_RELEASE += -s

QMAKE_CXXFLAGS_DEBUG += -DDEBUG_OUTPUT


DESTDIR = .
OBJECTS_DIR = _build/obj
UI_DIR = _build/uic
MOC_DIR = _build/moc
RCC_DIR = _build/rcc

//DEFINES = QT_NO_DEBUG_OUTPUT
LIBS += -lxkbfile -lX11
DEFINES += JRUGR_VERSION=\\\"0.4.7\\\"

include(src/main.pri)

RESOURCES += $$PWD/icons.qrc
